package middleware

import (
	"dmplab/internal/proto/clients/auth"
	"dmplab/internal/proto/clients/log"
	"dmplab/pkg/context"
	"encoding/json"
	"net/http"
	"strings"

	"time"
)

// Auth is a special middleware that check user token
func Auth(next http.Handler) http.Handler {
	authM := func(w http.ResponseWriter, r *http.Request) {

		if r.URL.Path == "/v1/auth" && (r.Method == "HEAD" || r.Method == "GET") {
			c, err := r.Cookie("dmp_remember_token")
			if err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}

			user, err := auth.ByRememberCtx(r.Context(), c.Value)
			if err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}

			if r.Method == "HEAD" {
				w.WriteHeader(http.StatusOK)
				return
			}

			if err := json.NewEncoder(w).Encode(user); err != nil {
				log.Errorf(0, "error encode json: %v", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			w.WriteHeader(http.StatusOK)
			return
		}

		// Normal flow
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(authM)
}

func RequireAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := context.User(r.Context())
		if user == nil {
			log.Error(0, "`RequireAdmin` requires `RequiredUser` before it")
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		if !user.Role.IsAdmin() {
			log.Warnf(int(user.ID), "tried to access '%s' (admin page)", r.RequestURI)
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func SetUserMw(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// if the user is requesting a static asset or image
		// we will not need to lookup the current user so we skip doing that.
		path := r.URL.Path
		if strings.HasPrefix(path, "/assets/") {
			next.ServeHTTP(w, r)
			return
		}
		// if the user is logged in..
		c, err := r.Cookie("dmp_remember_token")
		if err != nil {
			next.ServeHTTP(w, r)
			return
		}

		if c.Expires.Before(time.Now()) {
			c.Expires = time.Now().Add(time.Hour * 3)
		} else {
			next.ServeHTTP(w, r)
			return
		}

		user, err := auth.ByRememberCtx(r.Context(), c.Value)
		if err != nil {
			// http.Redirect(w, r, "/login", http.StatusFound)
			next.ServeHTTP(w, r)
			return
		}

		ctx := r.Context()
		ctx = context.WithUser(ctx, user)

		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

func RequiredUser(next http.Handler) http.Handler {
	ourHandler := func(w http.ResponseWriter, r *http.Request) {
		user := context.User(r.Context())
		if user == nil {
			log.Warnf(0, "anon tried to access page '%s'", r.RequestURI)
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(http.StatusText(http.StatusForbidden)))
			return
		}
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(ourHandler)
}
