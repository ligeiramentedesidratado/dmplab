### Dependencies
- docker
- go
- [go rice tool](https://github.com/GeertJohan/go.rice)
- node
- [jq](https://stedolan.github.io/jq)

### Usage
create a `.config` file inside `config` folder
```json
{
  "addr": "example.tld", // your website domain
  "env": "dev",
  "pepper": "SECRET",
  "hmac_key": "SECRET",
  "csrf_key": "SECRET",
  "ui": {
    "port": "3000",
    "host": "0.0.0.0"
  },
  "api": {
    "port": "3001",
    "host": "0.0.0.0"
  },
  "database": {
    "host": "dmp-db",
    "port": 5432,
    "user": "postgres",
    "pass": "123",
    "name": "dmplab_dev",
    "ssl": "disable"
  },

  "rrc": {
    "host": "0.0.0.0",
    "port": "3002",
    "rcaddr": "http://dmp-rclone:5572",
    "dst": "eb2:/", //Your rclone remote. This must match your rclone config
    "script": {
      "run": "./scripts/run.sh",
      "thumb": "_thumb.jpeg",
      "screen": "_screen.jpeg"
    }
  }
}
```

To set up a third-party service to host all encrypted videos,
you need to configure the rclone [crypt remote](https://rclone.org/crypt/) and
place all the information in a file called `rclone.conf` inside the` config` folder.

Make sure the remote that you choose has the ability to create public link.

In this example I'll be using BackBlaze b2:
```yaml
[custombackblaze]
type = b2
account = XXXXXXXXXXXXXXXXXXXXXXXXX
key = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
hard_delete = false

[eb2]
type = crypt
remote = backb:/bucketname/folder
filename_encryption = standard
directory_name_encryption = true
password = XXXXXXXXXXX
password2 = XXXXXXXXXXX
```

### TODO:

- [ ] unit test
- [ ] write and update documentation
- [ ] remove legacy code

