FROM alpine:3.11

RUN apk update \
        && apk upgrade \
        && apk add --no-cache libc6-compat

WORKDIR /srv/dmp

RUN addgroup -g 1000 dmp \
        && adduser -u 1000 -G dmp -s /bin/sh -D dmp \
        && mkdir -p /srv/dmp

COPY ./deploy/auth /srv/dmp/auth

RUN chown -R dmp:dmp /srv/dmp

EXPOSE 3005

CMD sleep 3 && ./auth

USER dmp
