FROM alpine:3.11

RUN apk update \
        && apk upgrade \
        && apk add --no-cache libc6-compat

WORKDIR /srv/dmp

RUN addgroup -g 1000 dmp \
        && adduser -u 1000 -G dmp -s /bin/sh -D dmp \
        && mkdir -p /srv/dmp \
        && mkdir -p /srv/dmp/tddata

COPY ./deploy/tusd /srv/dmp/

RUN chown -R dmp:dmp /srv/dmp
USER dmp:dmp

EXPOSE 1080

CMD ./tusd -verbose -behind-proxy -hooks-grpc "dmp-hook:3002" -hooks-enabled-events pre-create,post-create,post-finish -upload-dir tddata/
