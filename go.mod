module dmplab

go 1.13

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/render v1.0.1
	github.com/golang/protobuf v1.4.1
	github.com/gorilla/schema v1.1.0
	github.com/gorilla/websocket v1.4.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/jinzhu/gorm v1.9.12
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.1.1
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/prometheus/client_golang v1.6.0
	github.com/tus/tusd v1.3.0
	go.uber.org/zap v1.15.0
	golang.org/x/crypto v0.0.0-20200206161412-a0c6ece9d31a
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
	google.golang.org/appengine v1.6.5 // indirect
	google.golang.org/grpc v1.28.0
	google.golang.org/protobuf v1.24.0
)
