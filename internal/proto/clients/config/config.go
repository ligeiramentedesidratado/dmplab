package config

import (
	"context"
	"dmplab/internal/proto/contracts/config"

	"google.golang.org/grpc"
)

func init() {
	cfgc, err := newClientConfig()
	if err != nil {
		panic("failed to initialize config client: " + err.Error())
	}

	cfgClient = cfgc
}

var void = new(config.Void)

type configClient struct {
	conn       *grpc.ClientConn
	cfgService config.ConfigServiceClient
	config     *config.Config
	addrs      addrs
}

type addrs struct {
	DB       string
	LogDB    string
	API      string
	UI       string
	RCLONE   string
	RCLONERC string
}

var cfgClient *configClient
var cfg *configClient

func newClientConfig() (*configClient, error) {
	conn, err := grpc.Dial("dmp-config:3003", grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	cfgService := config.NewConfigServiceClient(conn)
	allcfg, err := cfgService.GetAllcfg(context.Background(), &config.Void{})
	if err != nil {
		panic(err)
	}
	client := &configClient{
		conn:       conn,
		cfgService: cfgService,
		config:     allcfg,
	}

	return client, nil
}

func GetDBAddr() string {
	if cfgClient.addrs.DB != "" {
		return cfgClient.addrs.DB
	}

	reply, err := cfgClient.cfgService.GetDBAddr(context.Background(), void)
	if err != nil {
		// TODO: PANIC IN PRODUCTION???
		panic(err)
	}

	cfgClient.addrs.DB = reply.GetAddr()
	return cfgClient.addrs.DB
}

func GetLogDBAddr() string {
	if cfgClient.addrs.LogDB != "" {
		return cfgClient.addrs.LogDB
	}
	reply, err := cfgClient.cfgService.GetLogDBAddr(context.Background(), void)
	if err != nil {
		// TODO: PANIC IN PRODUCTION???
		panic(err)
	}
	cfgClient.addrs.LogDB = reply.GetAddr()
	return cfgClient.addrs.LogDB

}

func GetAPIAddr() string {
	if cfgClient.addrs.API != "" {
		return cfgClient.addrs.API
	}
	reply, err := cfgClient.cfgService.GetAPIAddr(context.Background(), void)
	if err != nil {
		// TODO: PANIC IN PRODUCTION???
		panic(err)
	}
	cfgClient.addrs.API = reply.GetAddr()
	return cfgClient.addrs.API

}

func GetUIAddr() string {
	if cfgClient.addrs.UI != "" {
		return cfgClient.addrs.UI
	}
	reply, err := cfgClient.cfgService.GetUIAddr(context.Background(), void)
	if err != nil {
		// TODO: PANIC IN PRODUCTION???
		panic(err)
	}
	cfgClient.addrs.UI = reply.GetAddr()
	return cfgClient.addrs.UI

}

func GetRcloneAddr() string {
	if cfgClient.addrs.RCLONE != "" {
		return cfgClient.addrs.RCLONE
	}
	reply, err := cfgClient.cfgService.GetRcloneAddr(context.Background(), void)
	if err != nil {
		// TODO: PANIC IN PRODUCTION???
		panic(err)
	}
	cfgClient.addrs.RCLONE = reply.GetAddr()
	return cfgClient.addrs.RCLONE
}

func GetRcloneRemoteControlAddr() string {
	if cfgClient.addrs.RCLONERC != "" {
		return cfgClient.addrs.RCLONERC
	}
	reply, err := cfgClient.cfgService.GetRcloneRemoteControlAddr(context.Background(), void)
	if err != nil {
		// TODO: PANIC IN PRODUCTION???
		panic(err)
	}
	cfgClient.addrs.RCLONERC = reply.GetAddr()
	return cfgClient.addrs.RCLONERC
}

func GetAPIcfg() *config.AccessAddress {
	return cfgClient.config.GetAPI()

}

func GetUIcfg() *config.AccessAddress {
	return cfgClient.config.GetUI()
}

func GetInitcfg() *config.App {
	return cfgClient.config.GetApp()
}

func GetDatabasecfg() *config.Database {
	return cfgClient.config.GetDatabase()
}

func GetLogDatabasecfg() *config.Database {
	return cfgClient.config.GetLogDatabase()
}

func GetKeyscfg() *config.Keys {
	return cfgClient.config.GetKeys()
}

func GetRclonecfg() *config.Rclone {
	return cfgClient.config.GetRclone()
}

func GetRRCcfg() *config.RcloneRemoteControl {
	return cfgClient.config.Rclone.GetRRC()
}

func GetRScriptcfg() *config.RcloneScript {
	return cfgClient.config.Rclone.GetRScript()
}

func GetRcloneKeys() *config.RcloneKeys {
	return cfgClient.config.Rclone.GetRcloneKeys()
}

func Close() {
	cfgClient.conn.Close()
}
