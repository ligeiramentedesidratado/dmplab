package auth

import (
	"context"
	"dmplab/internal/models"
	"dmplab/internal/proto/contracts/auth"

	"google.golang.org/grpc"
)

func init() {
	aC, err := newAuthClient()
	if err != nil {
		panic(err)
	}

	authC = aC
}

type authClient struct {
	conn *grpc.ClientConn
	authService auth.AuthenticateServiceClient
}

var authC *authClient

func newAuthClient() (*authClient, error) {
	conn, err := grpc.Dial("dmp-auth:3005", grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	client := new(authClient)

	client.conn = conn
	client.authService = auth.NewAuthenticateServiceClient(conn)

	return client, nil
}

func ByRemember(token string) (*models.User, error) {
	return doByRemember(context.Background(), token)
}

func ByRememberCtx(ctx context.Context ,token string) (*models.User, error) {
	return doByRemember(ctx, token)
}

func doByRemember(ctx context.Context, token string) (*models.User, error) {
	req := &auth.AuthRequest{
		Token: token,
	}

	resp, err := authC.authService.ByRemember(ctx, req)
	if err != nil {
		return nil, err
	}

	user := new(models.User)

	user.ID = uint(resp.ID)
	user.Name = resp.Name
	user.Email = resp.Email
	user.RememberHash = resp.RememberHash
	user.Banned = resp.Banned
	user.DownRemaining = uint(resp.DownRemaining)
	user.Role = models.Role(resp.Role)

	return user, nil
}
