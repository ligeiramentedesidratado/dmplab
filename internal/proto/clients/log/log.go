package log

import (
	"context"
	"dmplab/internal/proto/contracts/logger"
	"runtime"
	"strconv"
	"strings"
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
)

func init() {
	lc, err := newlogClient()
	if err != nil {
		panic("failed to initialize log client: " + err.Error())
	}

	loggerClient = lc

	Info(0, "Logger initialized!")
}

type logClient struct {
	conn       *grpc.ClientConn
	logService logger.LoggerServiceClient
	zap        *zap.SugaredLogger
}

var loggerClient *logClient

func newlogClient() (*logClient, error) {
	conn, err := grpc.Dial("dmp-log:3004", grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	enc := zap.NewProductionEncoderConfig()
	enc.EncodeTime = zapcore.ISO8601TimeEncoder
	enc.EncodeCaller = shortCaller

	client := new(logClient)
	writer := zapcore.AddSync(client)

	core := zapcore.NewCore(zapcore.NewJSONEncoder(enc), writer, zapcore.DebugLevel)
	logg := zap.New(core, zap.AddCaller())

	client.conn = conn
	client.logService = logger.NewLoggerServiceClient(conn)
	client.zap = logg.Sugar()

	return client, nil
}

func (l *logClient) Write(p []byte) (n int, err error) {

	_, err = loggerClient.logService.SendLog(context.Background(), &logger.Message{P: p})
	return 0, err
}

func shortCaller(caller zapcore.EntryCaller, enc zapcore.PrimitiveArrayEncoder) {

	// Workaround.
	_, file, line, ok := runtime.Caller(7)

	if ok {
		strline := strconv.Itoa(line)
		idx := strings.LastIndexByte(file, '/')
		if idx == -1 {
			enc.AppendString(file + ":" + strline)
			return
		}
		idx = strings.LastIndexByte(file[:idx], '/')
		if idx == -1 {
			enc.AppendString(file + ":" + strline)
			return
		}

		enc.AppendString(file[idx+1:] + ":" + strline)
		return
	}

	enc.AppendString("???:0")
}

func RegisterService(service string) {
	loggerClient.zap = loggerClient.zap.With("service", service)
}

func Info(uid int, msg string) {
	// loggerClient.zap.Info(args...)
	loggerClient.zap.Infow(msg, "user", uid)
}

func Infof(uid int,templatestr string, args ...interface{}) {
	loggerClient.zap.Infow(fmt.Sprintf(templatestr, args...), "user", uid)
}

func Warn(uid int, msg string) {
	loggerClient.zap.Warnw(msg, "user", uid)
}

func Warnf(uid int, templatestr string, args ...interface{}) {
	loggerClient.zap.Warnw(fmt.Sprintf(templatestr, args...), "user", uid)
}

func Error(uid int, msg string) {
	loggerClient.zap.Errorw(msg, "user", uid)
}

func Errorf(uid int,templatestr string, args ...interface{}) {
	loggerClient.zap.Errorw(fmt.Sprintf(templatestr, args...), "user", uid)
}

func Debug(uid int, msg string) {
	loggerClient.zap.Debugw(msg, "user", uid)
}

func Debugf(uid int, templatestr string, args ...interface{}) {
	loggerClient.zap.Debugw(fmt.Sprintf(templatestr, args...), "user", uid)
}

func Close() {
	loggerClient.conn.Close()
}
