package main

import (
	"context"
	"dmplab/internal/models"
	"dmplab/internal/proto/clients/config"
	"dmplab/internal/proto/contracts/auth"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type authService struct {
	us models.UserService
}

func main() {
	listener, err := net.Listen("tcp", ":3005")
	if err != nil {
		panic(err)
	}
	defer listener.Close()
	gserver := grpc.NewServer()

	auththentication := newAuthService()

	auth.RegisterAuthenticateServiceServer(gserver, auththentication)

	if err := gserver.Serve(listener); err != nil {
		panic(err)
	}
}

func newAuthService() *authService {

	defer config.Close()

	mod, err := models.NewModelServices(
		models.WithDatabase(config.GetDatabasecfg().GetDialect(), config.GetDBAddr()),
		models.WithUser(config.GetKeyscfg().GetHMACKey(), config.GetKeyscfg().GetPepper()),
	)
	if err != nil {
		panic(err)
	}

	auththentication := &authService{
		us: mod.User,
	}

	return auththentication
}

func (a *authService) ByRemember(ctx context.Context, req *auth.AuthRequest) (*auth.AuthResponse, error) {

	token := req.GetToken()
	if token == "" {
		return nil, status.Error(codes.InvalidArgument, "token not present")
	}

	user, err := a.us.ByRemember(token)
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, err.Error())
	}

	response := new(auth.AuthResponse)

	response.ID = uint32(user.ID)
	response.Role = int32(user.Role)
	response.DownRemaining = uint32(user.DownRemaining)
	response.Name = user.Name
	response.Email = user.Email
	response.RememberHash = user.RememberHash
	response.Banned = user.Banned

	return response, nil
}
