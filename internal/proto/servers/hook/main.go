package main

import (
	"dmplab/internal/errors"
	"dmplab/internal/models"
	"dmplab/internal/proto/clients/auth"
	"dmplab/internal/proto/clients/config"
	"dmplab/internal/proto/clients/log"
	"dmplab/internal/proto/contracts/hook"
	"time"

	"context"
	"encoding/hex"
	"fmt"
	"net"
	"net/http"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var metaDatas = []string{
	"title",
	"desc",
	"tags",
	"filename",
	"md5",
}

type hookService struct {
	hook.HookServiceServer
	vs   models.VideoService
	rrcs models.RRCService
}

func unaryInterceptor(ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (resp interface{}, err error) {

	fmt.Println("SERVER: ")
	fmt.Println("<---unaryinterceptor", info.FullMethod)
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		for k, v := range md {
			fmt.Printf("%v: %v\n", k, v)
		}
	}
	fmt.Println("outgoing")
	out, pass := metadata.FromOutgoingContext(ctx)
	if pass {
		for k, v := range out {
			fmt.Printf("%v: %v\n", k, v)
	}
}
	fmt.Println("unaryinterceptor--->")
	return handler(ctx, req)
}

func main() {
	log.RegisterService("hook")
	defer log.Close()

	listener, err := net.Listen("tcp", ":3002")
	if err != nil {
		panic(err)
	}
	defer listener.Close()
	gserver := grpc.NewServer(
		grpc.UnaryInterceptor(unaryInterceptor),
	)

	hooks := new(hookService)

	hook.RegisterHookServiceServer(gserver, hooks)

	mod, err := models.NewModelServices(
		models.WithDatabase(config.GetDatabasecfg().GetDialect(), config.GetDBAddr()),
		models.WithVideo(),
		models.WithRRC(),
	)
	if err != nil {
		panic(err)
	}

	hooks.vs = mod.Video
	hooks.rrcs = mod.RRC

	go hooks.periodicWorker()

	if err := gserver.Serve(listener); err != nil {
		panic(err)
	}
}

func (h *hookService) Send(ctx context.Context, req *hook.SendRequest) (*hook.SendResponse, error) {

	header := http.Header{}
	header.Add("Cookie", req.Hook.HttpRequest.Cookies[0])
	request := http.Request{Header: header}
	cookie, err := request.Cookie("dmp_remember_token")
	if err != nil {
		log.Warn(0, "invalid cookie")
		return nil, status.Errorf(codes.Internal, errors.GenericError.Error())
	}

	user, err := auth.ByRememberCtx(ctx, cookie.Value)
	if err != nil {
		log.Warn(0, "invalid cookie")
		return nil, status.Errorf(codes.Unauthenticated, errors.GetError(err))
	}

	if !user.Role.CanCreate() {
		log.Warn(int(user.ID), "not have permission to create a new file")

		return nil, status.Errorf(codes.PermissionDenied, "you don't have permission to upload a file")
	}

	switch req.Hook.Name {
	case "pre-create":
		log.Info(int(user.ID), "started a new upload")
		return h.preCreate(ctx, req, user)

	case "post-create":
		return h.postCreate(req, user)

	case "post-receive":
		//Do nothing
		log.Info(0, "POST RECEIVE")
	case "post-finish":
		//This event is triggered after the upload is fully finished, meaning that the file
		// is already for further processing

		//save the MD5 metadata that came through the tusd request in the database
		return h.postFinish(ctx, req, user)
	case "post-terminate":
		//Do nothing
		log.Info(0, "POST TERMINATE")
	default:
		log.Error(0, "UNKNOWN: "+req.Hook.Name)
	}

	return &hook.SendResponse{}, nil
}

func (h *hookService) preCreate(ctx context.Context, req *hook.SendRequest, user *models.User) (*hook.SendResponse, error) {

	var video *models.Video
	var err error

	reliableMetaData := make(map[string]string)

	for _, key := range metaDatas {
		val, ok := req.Hook.Upload.MetaData[key]
		if !ok {
			log.Errorf(int(user.ID), "tried to upload with missing metadata: %s", key)
			return nil, status.Errorf(codes.Internal, errors.GenericError.Error())
		}
		reliableMetaData[key] = val
	}

	hash := reliableMetaData["md5"]
	bs, err := hex.DecodeString(hash)
	if err != nil {
		log.Errorf(0, "error trying to decode md5: %s. %v", hash, err)
		return nil, status.Errorf(codes.Internal, errors.GenericError.Error())
	}

	video, err = h.vs.ByRember(bs)
	if err != nil {
		if errors.Is(err, errors.ErrNotFound) {
			video = &models.Video{
				Title:       reliableMetaData["title"],
				Filename:    reliableMetaData["filename"],
				Description: reliableMetaData["desc"],
				Tags:        strings.Split(reliableMetaData["tags"], ","),
				Md5:         bs,
				TusdID:      req.Hook.Upload.Id,
				Size:        uint64(req.Hook.Upload.Size),
				UserID:      user.ID,
				Status:      models.PreCreate,
			}

			if err := h.vs.Create(video); err != nil {
				log.Errorf(int(user.ID), "error while creating a new file: %v", err)
				return nil, status.Errorf(codes.Unknown, errors.GetError(err))
			}

			return &hook.SendResponse{}, nil
		}

		log.Errorf(0, "error while getting file by md5: %v", err)
		return nil, status.Errorf(codes.Internal, errors.GetError(err))
	}

	if video.UserID != user.ID && video.Status >= models.Create {
		log.Errorf(int(user.ID), "tried to upload an existing file %s", string(hash))
		return nil, status.Errorf(codes.AlreadyExists, "This file already exists!")
	}
	if video.UserID == user.ID && video.Status >= models.Done {
		log.Errorf(int(user.ID), "tried to upload an already own file %s", string(hash))
		return nil, status.Errorf(codes.AlreadyExists, "You already own this file!")
	}

	return &hook.SendResponse{}, nil
}

func (h *hookService) postCreate(req *hook.SendRequest, user *models.User) (*hook.SendResponse, error) {

	md5, ok := req.Hook.Upload.MetaData["md5"]
	if !ok {
		log.Error(int(user.ID), "tried to upload with missing metadata: md5")
		return nil, status.Errorf(codes.Internal, errors.GenericError.Error())
	}

	bs, err := hex.DecodeString(md5)
	if err != nil {
		log.Errorf(0, "error trying to decode md5: %s: %v", md5, err)
		return nil, status.Errorf(codes.Internal, errors.GenericError.Error())
	}

	video, err := h.vs.ByRember(bs)
	if err != nil {
		log.Errorf(int(user.ID), "error getting file from db: %v", err)
		return nil, status.Errorf(codes.Unknown, errors.GetError(err))
	}

	video.TusdID = req.Hook.Upload.GetId()

	if err := h.vs.UpdateTusdID(video); err != nil {
		return nil, errors.Wrap(err, "error while updating status")
	}

	return &hook.SendResponse{}, nil
}

func (h *hookService) postFinish(ctx context.Context, req *hook.SendRequest, user *models.User) (*hook.SendResponse, error) {

	md5, ok := req.Hook.Upload.MetaData["md5"]
	if !ok {
		log.Error(int(user.ID), "tried to upload with missing metadata: md5")
		return nil, status.Errorf(codes.Internal, errors.GenericError.Error())
	}

	bs, err := hex.DecodeString(md5)
	if err != nil {
		log.Errorf(0, "error trying to decode md5: %s. %v", md5, err)
		return nil, status.Errorf(codes.Internal, errors.GenericError.Error())
	}

	video, err := h.vs.ByRember(bs)
	if err != nil {
		log.Errorf(int(user.ID), "error getting file from db: %v", err)
		return nil, status.Errorf(codes.Unknown, errors.GetError(err))
	}

	video.TusdID = req.Hook.Upload.GetId()

	go func() {
		defer func() {
			if err := recover(); err != nil {
				log.Errorf(0, "recover from panicking: %v", err)
			}
		}()

		if err := h.worker(video); err != nil {
			log.Errorf(0, "worker failed due: %v", err)
			return
		}

		log.Infof(int(user.ID), "a new file was uploaded successfully: %s", video.Title)
	}()

	return &hook.SendResponse{}, nil
}

func (h *hookService) periodicWorker() {
	for {

		time.Sleep(30 * time.Minute)

		videos, err := h.vs.ByNotDone()
		if err != nil {
			log.Errorf(0, "periodic worker failed: %v", err)
			continue
		}

		for _, video := range videos {
			if err := h.worker(&video); err != nil {
				log.Errorf(0, "manual intervention is necessary for the video: %d: %v", video.ID, err)
			}
		}

	}
}

func (h *hookService) worker(v *models.Video) error {

	for v.Status != models.Done {

		switch v.Status {

		// will use Rclone Remote Control to upload to the third party server
		case models.PreCreate:

			v.Status = models.Create
			if err := h.rrcs.Create(v); err != nil {
				return errors.Wrap(err, "failed while creating in the remote rclone")
			}

		// get all links: video, thumbnal and screen
		case models.Create:

			v.Status = models.URL
			if err := h.rrcs.GetPubLink(v); err != nil {
				return errors.Wrap(err, "failed while getting public links")
			}

		// claims disk space by deleting the video and update status to `Done`
		case models.URL:

			v.Status = models.Done
			if err := h.rrcs.Clear(v); err != nil {
				return errors.Wrap(err, "failed while cleaning files")
			}

			if err := h.vs.Update(v); err != nil {
				return errors.Wrap(err, "failed while updating status to `Done`")
			}

		default:
			return errors.ErrStatusInvalid
		}

	}
	return nil
}
