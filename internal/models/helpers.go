package models

import (
	"crypto/md5"
	"encoding/hex"
	"dmplab/internal/errors"
	"path"
	"regexp"
	"strings"
	"time"
)

const (
	data = "tddata/"
)

var (
	valueIndex int
	arrayRegex *regexp.Regexp
)

func init() {
	arrayRegex = regexp.MustCompile(`(((?P<value>(([^",\\{}\s(NULL)])+|"([^"\\]|\\"|\\\\)*")))(,)?)`)
	for i, subexp := range arrayRegex.SubexpNames() {
		if subexp == "value" {
			valueIndex = i
			break
		}
	}
}

type YieldPayload map[string]interface{}

func (array YieldPayload) ParseArray(key string) {
	if value, ok := array[key]; ok {
		if bsTags, ok := value.([]byte); ok {
			if len(bsTags) <= 2 {
				delete(array, key)
				return
			}

			tags := make([]string, 0)
			matches := arrayRegex.FindAllStringSubmatch(string(bsTags), -1)
			for _, match := range matches {
				s := match[valueIndex]
				s = strings.Trim(s, "\"")
				tags = append(tags, s)
			}
			array[key] = tags
		}
	}

}

// Model is a copy of gorm.Model but with some json tags
type Model struct {
	ID        uint       `db:"id" json:"id,omitempty"`
	CreatedAt time.Time  `db:"created_at" json:"created,omitempty"`
	UpdatedAt time.Time  `db:"updated_at" json:"-,omitempty"`
	DeletedAt *time.Time `db:"deleted_at" json:"-"`
}

// getFile will return the file name saved in the disk
// i.e : f95fb35ac69feeda452c7354b772252a
func getFile(v *Video) (string, error) {

	if v.TusdID == "" {
		return "", errors.ErrTusdIDRequired
	}
	return v.TusdID, nil
}

// getFile will return the file info name saved in the disk
// i.e : f95fb35ac69feeda452c7354b772252a
func getFileInfo(v *Video) (string, error) {

	if v.TusdID == "" {
		return "", errors.ErrTusdIDRequired
	}
	return v.TusdID + ".info", nil
}

// getFileWithoutDataPrfix will return the folder/filename
// i.e: 7eb06281330e967f6982f58574aef988/f95fb35ac69feeda452c7354b772252a
func getFileWithoutDataPrfix(v *Video) (string, error) {
	fname, err := getFile(v)
	if err != nil {
		return "", err
	}

	return path.Join(hex.EncodeToString(v.Md5), fname), nil
}

// getFileLocation will return the data/filename
// i.e: data/f95fb35ac69feeda452c7354b772252a
func getFileLocation(v *Video) (string, error) {
	fname, err := getFile(v)
	if err != nil {
		return "", err
	}

	return path.Join(data, fname), nil
}

// getDir will return data/folder
// i.e: data/7eb06281330e967f6982f58574aef988
func getDir(v *Video) (string, error) {
	if len(v.Md5) != md5.Size {
		return "", errors.ErrMD5Invalid
	}
	return path.Join(data, hex.EncodeToString(v.Md5)), nil
}

//getFinalFileLocation will return data/folder/filename
// i.e: data/7eb06281330e967f6982f58574aef988/f95fb35ac69feeda452c7354b772252a
func getFinalFileLocation(v *Video) (string, error) {
	dir, err := getDir(v)
	if err != nil {
		return "", err
	}
	fname, err := getFile(v)
	if err != nil {
		return "", err
	}

	return path.Join(dir, fname), nil
}
