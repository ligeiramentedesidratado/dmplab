package models

import (
	"reflect"
	"testing"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/jmoiron/sqlx"
)

type videoTestTable struct {
	name    string
	video   *Video
	wantErr bool
	want    string
}

func Test_newVideoService(t *testing.T) {
	type args struct {
		db *sqlx.DB
	}
	tests := []struct {
		name string
		args args
		want VideoService
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newVideoService(tt.args.db); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newVideoService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_videoValidator_ByUserID(t *testing.T) {
	type fields struct {
		VideoDB VideoDB
	}
	type args struct {
		userID uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []Video
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{
				VideoDB: tt.fields.VideoDB,
			}
			got, err := vv.ByUserID(tt.args.userID)
			if (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.ByUserID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("videoValidator.ByUserID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_videoValidator_ByID(t *testing.T) {
	type fields struct {
		VideoDB VideoDB
	}
	type args struct {
		id uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Video
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{
				VideoDB: tt.fields.VideoDB,
			}
			got, err := vv.ByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.ByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("videoValidator.ByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_videoValidator_ByRember(t *testing.T) {
	type fields struct {
		VideoDB VideoDB
	}
	type args struct {
		md5 string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Video
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{
				VideoDB: tt.fields.VideoDB,
			}
			got, err := vv.ByRember(tt.args.md5)
			if (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.ByRember() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("videoValidator.ByRember() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_videoValidator_Create(t *testing.T) {
	type fields struct {
		VideoDB VideoDB
	}
	type args struct {
		v *Video
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{
				VideoDB: tt.fields.VideoDB,
			}
			if err := vv.Create(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_videoValidator_UpdateDone(t *testing.T) {
	type fields struct {
		VideoDB VideoDB
	}
	type args struct {
		v *Video
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{
				VideoDB: tt.fields.VideoDB,
			}
			if err := vv.UpdateDone(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.UpdateDone() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_videoValidator_filenameRequired(t *testing.T) {

	type fields struct {
		VideoDB VideoDB
	}
	type args struct {
		v *Video
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{name: "Valid Filename", fields: fields{nil}, args: args{&Video{Filename: "Simple$$Filename.mp4"}}, wantErr: false},
		{name: "Empty Filename", fields: fields{nil}, args: args{&Video{Filename: ""}}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{
				VideoDB: tt.fields.VideoDB,
			}
			if err := vv.filenameRequired(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.filenameRequired() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_videoValidator_filenameLess128(t *testing.T) {

	tests := []videoTestTable{
		{name: "Valid short filename", video: &Video{Filename: safeFilename(32)}, wantErr: false},
		{name: "Valid large filename", video: &Video{Filename: safeFilename(127)}, wantErr: false},
		{name: "Invalid large filename", video: &Video{Filename: safeFilename(128)}, wantErr: true},
		{name: "Invalid very large filename", video: &Video{Filename: safeFilename(256)}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{}
			if err := vv.filenameLess128(tt.video); (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.filenameLess128() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_videoValidator_rememberHashRequired(t *testing.T) {

	tests := []videoTestTable{
		{name: "Valid md5", video: &Video{Md5: []byte("9e107d9d372bb6826bd81d3542a419d6")}, wantErr: false},
		{name: "Valid with white space", video: &Video{Md5: []byte(" 9e107d9d372bb6826bd81d3542a419d6")}, wantErr: false},
		{name: "Invalid hex character", video: &Video{Md5: []byte("Ze107d9d372bb6826bd81d3542a419d6")}, wantErr: true},
		{name: "Invalid too short", video: &Video{Md5: []byte("9e107d9d372bb682")}, wantErr: true},
		{name: "Invalid too large", video: &Video{Md5: []byte("9e107d9d372bb6826bd81d3542a419d69e107d9d372bb6")}, wantErr: true},
		{name: "Invalid empty", video: &Video{Md5: []byte("")}, wantErr: true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{}
			if err := vv.rememberHashRequired(tt.video); (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.rememberHashRequired() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_videoValidator_sizeRequired(t *testing.T) {

	tests := []videoTestTable{
		{name: "Invalid size", video: &Video{Size: 0}, wantErr: true},
		{name: "Valid size", video: &Video{Size: 1024}, wantErr: false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{}
			if err := vv.sizeRequired(tt.video); (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.sizeRequired() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_videoValidator_userIDRequired(t *testing.T) {

	tests := []videoTestTable{
		{name: "Invalid userID", video: &Video{Size: 0}, wantErr: true},
		{name: "Valid userID", video: &Video{UserID: 1}, wantErr: false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vv := &videoValidator{}
			if err := vv.userIDRequired(tt.video); (err != nil) != tt.wantErr {
				t.Errorf("videoValidator.userIDRequired() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_videoDB_ByUserID(t *testing.T) {
	defer db.teardown(t)
	stmt := `INSERT INTO videos (created_at,
								 updated_at,
								 deleted_at,
								 filename,
								 title,
								 description,
								 tags,
								 md5,
								 size,
								 status,
								 tusd_id,
								 video_pub_link,
								 screen_pub_link,
								 thumb_pub_link,
								 user_id)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)`

	_, err := db.testdb.Exec(
		stmt,
		time.Now(),
		time.Now(),
		nil,
		validVideo.Filename,
		validVideo.Title,
		validVideo.Description,
		validVideo.Tags,
		validVideo.Md5,
		validVideo.Size,
		validVideo.Status,
		validVideo.TusdID,
		validVideo.VideoPubLink,
		validVideo.ScreenPubLink,
		validVideo.ThumbPubLink,
		validVideo.UserID,
	)
	if err != nil {
		t.Fatal(err)
	}

	want := []Video{
		*validVideo,
	}

	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		userID uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []Video
		wantErr bool
	}{
		{name: "Valid query", args: args{1}, fields: fields{db: db.testdb}, want: want, wantErr: false},
		{name: "Inalid query", args: args{0}, fields: fields{db: db.testdb}, want: []Video{}, wantErr: false},
		{name: "Inalid non existing id", args: args{2}, fields: fields{db: db.testdb}, want: []Video{}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vg := &videoDB{
				db: tt.fields.db,
			}
			got, err := vg.ByUserID(tt.args.userID)
			if (err != nil) != tt.wantErr {
				t.Errorf("videoGorm.ByUserID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// hack way
			if len(got) > 0 {
				got[0].CreatedAt = want[0].CreatedAt
				got[0].UpdatedAt = want[0].UpdatedAt
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("videoGorm.ByUserID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_videoGorm_ByID(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		id uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Video
		wantErr bool
	}{
		// TODO: Add test cases.

	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vg := &videoDB{
				db: tt.fields.db,
			}
			got, err := vg.ByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("videoGorm.ByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("videoGorm.ByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_videoGorm_ByRember(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		md5 string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Video
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vg := &videoDB{
				db: tt.fields.db,
			}
			got, err := vg.ByRember(tt.args.md5)
			if (err != nil) != tt.wantErr {
				t.Errorf("videoGorm.ByRember() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("videoGorm.ByRember() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_videoGorm_Create(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		v *Video
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vg := &videoDB{
				db: tt.fields.db,
			}
			if err := vg.Create(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("videoGorm.Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_videoGorm_Update(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		v *Video
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vg := &videoDB{
				db: tt.fields.db,
			}
			if err := vg.Update(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("videoGorm.Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_videoGorm_UpdateStatus(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		v *Video
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vg := &videoDB{
				db: tt.fields.db,
			}
			if err := vg.UpdateStatus(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("videoGorm.UpdateStatus() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
