package models

import (
	"dmplab/pkg/hash"
	"reflect"
	"regexp"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func Test_userService_Authenticate(t *testing.T) {
	type fields struct {
		UserDB UserDB
		pepper string
	}
	type args struct {
		email    string
		password string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			us := &userService{
				UserDB: tt.fields.UserDB,
				pepper: tt.fields.pepper,
			}
			got, err := us.Authenticate(tt.args.email, tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("userService.Authenticate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("userService.Authenticate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_userValidator_ByEmail(t *testing.T) {
	type fields struct {
		UserDB     UserDB
		hmac       hash.HMAC
		emailRegex *regexp.Regexp
		pepper     string
	}
	type args struct {
		email string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uv := &userValidator{
				UserDB:     tt.fields.UserDB,
				hmac:       tt.fields.hmac,
				emailRegex: tt.fields.emailRegex,
				pepper:     tt.fields.pepper,
			}
			got, err := uv.ByEmail(tt.args.email)
			if (err != nil) != tt.wantErr {
				t.Errorf("userValidator.ByEmail() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("userValidator.ByEmail() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_userValidator_ByRemember(t *testing.T) {
	type fields struct {
		UserDB     UserDB
		hmac       hash.HMAC
		emailRegex *regexp.Regexp
		pepper     string
	}
	type args struct {
		token string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uv := &userValidator{
				UserDB:     tt.fields.UserDB,
				hmac:       tt.fields.hmac,
				emailRegex: tt.fields.emailRegex,
				pepper:     tt.fields.pepper,
			}
			got, err := uv.ByRemember(tt.args.token)
			if (err != nil) != tt.wantErr {
				t.Errorf("userValidator.ByRemember() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("userValidator.ByRemember() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_userValidator_Create(t *testing.T) {
	type fields struct {
		UserDB     UserDB
		hmac       hash.HMAC
		emailRegex *regexp.Regexp
		pepper     string
	}
	type args struct {
		user *User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uv := &userValidator{
				UserDB:     tt.fields.UserDB,
				hmac:       tt.fields.hmac,
				emailRegex: tt.fields.emailRegex,
				pepper:     tt.fields.pepper,
			}
			if err := uv.Create(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("userValidator.Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_userValidator_Update(t *testing.T) {
	type fields struct {
		UserDB     UserDB
		hmac       hash.HMAC
		emailRegex *regexp.Regexp
		pepper     string
	}
	type args struct {
		user *User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uv := &userValidator{
				UserDB:     tt.fields.UserDB,
				hmac:       tt.fields.hmac,
				emailRegex: tt.fields.emailRegex,
				pepper:     tt.fields.pepper,
			}
			if err := uv.Update(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("userValidator.Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_userValidator_Delete(t *testing.T) {
	type fields struct {
		UserDB     UserDB
		hmac       hash.HMAC
		emailRegex *regexp.Regexp
		pepper     string
	}
	type args struct {
		id uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uv := &userValidator{
				UserDB:     tt.fields.UserDB,
				hmac:       tt.fields.hmac,
				emailRegex: tt.fields.emailRegex,
				pepper:     tt.fields.pepper,
			}
			if err := uv.Delete(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("userValidator.Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_userGorm_ByID(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		id uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ug := &userDB{
				db: tt.fields.db,
			}
			got, err := ug.ByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("userGorm.ByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("userGorm.ByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_userGorm_ByEmail(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		email string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ug := &userDB{
				db: tt.fields.db,
			}
			got, err := ug.ByEmail(tt.args.email)
			if (err != nil) != tt.wantErr {
				t.Errorf("userGorm.ByEmail() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("userGorm.ByEmail() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_userGorm_ByRemember(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		rememberHash string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ug := &userDB{
				db: tt.fields.db,
			}
			got, err := ug.ByRemember(tt.args.rememberHash)
			if (err != nil) != tt.wantErr {
				t.Errorf("userGorm.ByRemember() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("userGorm.ByRemember() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_userGorm_Create(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		user *User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ug := &userDB{
				db: tt.fields.db,
			}
			if err := ug.Create(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("userGorm.Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_userGorm_Update(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		user *User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ug := &userDB{
				db: tt.fields.db,
			}
			if err := ug.Update(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("userGorm.Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_userGorm_Delete(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		id uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ug := &userDB{
				db: tt.fields.db,
			}
			if err := ug.Delete(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("userGorm.Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
