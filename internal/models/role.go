package models

import (
	"dmplab/internal/errors"
)

type Role int

const (
	READ = Role(1) << iota
	READ_ANY
	CREATE
	EDIT_OWN
	DELETE_OWN
	EDIT_ANY
	DELETE_ANY
)

const (
	BASIC_PERM        = READ
	UPLOADER_PERM     = READ | CREATE | EDIT_OWN | DELETE_OWN
	VIP_PERM          = READ | READ_ANY
	VIP_UPLOADER_PERM = READ | READ_ANY | CREATE | EDIT_OWN | DELETE_OWN
	MODERATOR_PERM    = READ | EDIT_ANY
	ADMIN_PERM        = READ | READ_ANY | CREATE | EDIT_OWN | DELETE_OWN | EDIT_ANY | DELETE_ANY
)

func (r Role) String() string {
	str := "Can"

	if r.CanRead()      { str += " Read" }
	if r.CanReadAny()   { str += " Read_any" }
	if r.CanCreate()    { str += " Create" }
	if r.CanEdit()      { str += " Edit_own" }
	if r.CanDelete()    { str += " Delete_own" }
	if r.CanEditAny()   { str += " Edit_any" }
	if r.CanDeleteAny() { str += " Delete_any" }

	return str
}

func (r Role) CanRead() bool      { return r&READ != 0 }
func (r Role) CanReadAny() bool   { return r&READ_ANY != 0 }
func (r Role) CanCreate() bool    { return r&CREATE != 0 }
func (r Role) CanEdit() bool      { return r&EDIT_OWN != 0 }
func (r Role) CanEditAny() bool   { return r&EDIT_ANY != 0 }
func (r Role) CanDelete() bool    { return r&DELETE_OWN != 0 }
func (r Role) CanDeleteAny() bool { return r&DELETE_ANY != 0 }

func (r Role) IsBasic() bool       { return r == BASIC_PERM }
func (r Role) IsUploader() bool    { return r == UPLOADER_PERM }
func (r Role) IsVip() bool         { return r == VIP_PERM }
func (r Role) IsVipUploader() bool { return r == VIP_UPLOADER_PERM }
func (r Role) IsModerator() bool   { return r == MODERATOR_PERM }
func (r Role) IsAdmin() bool       { return r == ADMIN_PERM }

func (r Role) GetRole() (Role, error) {

	switch {
	case r.IsAdmin():       return ADMIN_PERM, nil
	case r.IsModerator():   return MODERATOR_PERM, nil
	case r.IsVipUploader(): return VIP_UPLOADER_PERM, nil
	case r.IsVip():         return VIP_PERM, nil
	case r.IsUploader():    return UPLOADER_PERM, nil
	case r.IsBasic():       return BASIC_PERM, nil
	default:                return 0, errors.ErrRoleInvalid
	}
}
