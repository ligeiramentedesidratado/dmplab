package models

import (
	"bytes"
	"dmplab/internal/proto/clients/config"
	"dmplab/internal/errors"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path"
)

type RcloneRC interface {
	// Create will try to save the video and thumbnails in a third party service
	Create(v *Video) error
	// GetPubLink will try to return a public link for the file, thumbnail and screen or an error
	GetPubLink(v *Video) error
	// Clear will remove the provided video from disk if the status >= Done
	Clear(v *Video) error
}

func newRRCService() RRCService {

	return &rrcService{
		RcloneRC: &rrcValidator{
			RcloneRC: &rcloneRemoteControl{}}}
}

type (
	RRCService interface {
		RcloneRC
	}

	rrcService struct {
		RcloneRC
	}

	rrcValidator struct {
		RcloneRC
	}
)

// createThumbAndScreen will use an external script to generate thumbanail
// and screen of the provided video
func (rrc *rrcValidator) createThumbAndScreen(v *Video) error {
	finalf, err := getFinalFileLocation(v)
	if err != nil {
		return err
	}

	script := config.GetRScriptcfg()
	cmd := exec.Command(script.GetRun(),
		finalf,             // input file
		finalf,             // output file
		script.GetThumb(),  // thumbnail postfix
		script.GetScreen(), // screen postfix
	)

	out, err := cmd.CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed due: %s", string(out))
	}

	return nil
}

// moveFile will move a file to an folder grouping all the generated files
// thumbnail, screen, info etc...
func (rrc *rrcValidator) moveFile(v *Video) error {

	dir, err := getDir(v)
	if err != nil {
		return err
	}
	flocal, err := getFileLocation(v)
	if err != nil {
		return err
	}
	ffinalloc, err := getFinalFileLocation(v)
	if err != nil {
		return err
	}
	ffinfo, err := getFileInfo(v)
	if err != nil {
		return err
	}

	if err := os.MkdirAll(dir, 0755); err != nil {
		return err
	}

	if err := os.Rename(path.Join(data, ffinfo), path.Join(dir, ffinfo)); err != nil {
		return err
	}

	err = os.Rename(flocal, ffinalloc)
	if err != nil {
		return err
	}
	return nil
}

func (rrc *rrcValidator) Create(v *Video) error {
	if err := runRRCValFuncs(v,
		rrc.moveFile,
		rrc.createThumbAndScreen,
	); err != nil {
		return err
	}

	return rrc.RcloneRC.Create(v)
}

func (rrc *rrcValidator) statusDone(v *Video) error {
	if v.Status < Done {
		return errors.ErrNotDone
	}
	return nil
}

func (rrc *rrcValidator) Clear(v *Video) error {
	if err := runRRCValFuncs(v,
		rrc.statusDone,
	); err != nil {
		return err
	}

	return rrc.RcloneRC.Clear(v)
}

type rrcValFunc func(*Video) error

func runRRCValFuncs(v *Video, fns ...rrcValFunc) error {
	for _, fn := range fns {
		if err := fn(v); err != nil {
			return err
		}
	}
	return nil
}

var _ RcloneRC = &rcloneRemoteControl{}

type rcloneRemoteControl struct{}

func (rrc *rcloneRemoteControl) Create(v *Video) error {
	wddir, err := os.Getwd()
	if err != nil {
		return err
	}
	dir, err := getDir(v)
	if err != nil {
		return err
	}
	srcfs := path.Join(wddir, dir)

	dst := config.GetRRCcfg()
	rBody, err := json.Marshal(map[string]string{
		"srcFs": srcfs,
		"dstFs": dst.GetDst() + hex.EncodeToString(v.Md5),
	})
	if err != nil {
		return err
	}

	req, err := http.Post(config.GetRcloneRemoteControlAddr()+"/sync/copy", "application/json", bytes.NewBuffer(rBody))
	if err != nil {
		return err
	}
	defer req.Body.Close()
	bs, err := ioutil.ReadAll(req.Body)
	fmt.Println(string(bs))
	return nil
}

func (rrc *rcloneRemoteControl) GetPubLink(v *Video) error {

	files := [3]string{"",
	config.GetRclonecfg().RScript.GetThumb(),
	config.GetRclonecfg().RScript.GetScreen(),
}

	fdir, err := getFileWithoutDataPrfix(v)
	if err != nil {
		return err
	}

	for i, f := range files {
		rBody, err := json.Marshal(map[string]string{
			"fs":     config.GetRclonecfg().GetRRC().GetDst(),
			"remote": fdir + f,
		})
		if err != nil {
			return err
		}
		resp, err := http.Post(config.GetRcloneRemoteControlAddr()+"/operations/publiclink", "application/json", bytes.NewBuffer(rBody))
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		url, err := getURL(resp)
		if err != nil {
			return err
		}

		if i == 0 {
			v.VideoPubLink = url
			continue
		}
		if i == 1 {
			v.ThumbPubLink = url
			continue
		}
		if i == 2 {
			v.ScreenPubLink = url
		}

	}
	return nil
}

func (rrc *rcloneRemoteControl) Clear(v *Video) error {
	dir, err := getDir(v)
	if err != nil {
		return err
	}
	return os.RemoveAll(dir)
}

func getURL(resp *http.Response) (string, error) {
	var fileURL map[string]interface{}
	// Decode json response
	if err := json.NewDecoder(resp.Body).Decode(&fileURL); err != nil {
		return "", err
	}
	// check errors in the response
	if err, ok := fileURL["error"]; ok {
		return "", errors.NewPriveteError(err.(string))
	}

	if url, ok := fileURL["url"]; ok {
		return url.(string), nil
	} else {
		return "", errors.NewPriveteError("something is really really wrong! url not received")
	}
}
