package models

import (
	"database/sql"
	"dmplab/internal/errors"
	"dmplab/pkg/argon2id"
	"dmplab/pkg/hash"
	"dmplab/pkg/rand"
	"fmt"
	"regexp"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/jmoiron/sqlx"
)

type User struct {
	Model
	Name          string `db:"name" json:"name,omitempty"`
	Email         string `db:"email" json:"email,omitempty"`
	Password      string `db:"-" json:"-"`
	PasswordHash  string `db:"password_hash" json:"-"`
	Remember      string `db:"-" json:"-"`
	RememberHash  string `db:"remember_hash" json:"-"`
	Banned        bool   `db:"-" json:"banned"`
	Role          Role   `db:"role" json:"role"`
	DownRemaining uint   `db:"down_remaining" json:"remaining"`
}

type UserYield interface {
	DashboardYield(user *User) ([]YieldPayload, error)
}

// UserDB is used to interact with the users db
type UserDB interface {
	// Methods for queryng for single user
	ByID(id uint) (*User, error)
	ByEmail(email string) (*User, error)
	ByRemember(token string) (*User, error)

	//Methods for altering users
	Create(user *User) error
	Update(user *User) error
	UpdateRememberHash(user *User) error
	UpdateRole(user *User) error
	UpdateDownRemaining(user *User) error
	AdminUpdateUser(user *User) error
	Delete(id uint) error

	//All sophisticated CRUD goes here
	UserYield
}

// UserService is a set of method used to manipulate and
// work with the user model
type UserService interface {
	//Authenticate will verify the provided email addres email and password are correct.
	//if they are correct, the user corresponding to that email will be returned.
	// Otherwise you will recieve either:
	// ErrNotFound, ErrPwdIncorrect, or another error if something goes wrong
	Authenticate(email, password string) (*User, error)
	UserDB
}

func newUserService(db *sqlx.DB, hmacKey, pepper string) UserService {
	ug := &userDB{db}
	hmac := hash.NewHMAC(hmacKey)
	uv := newUserValidator(ug, hmac, pepper)

	return &userService{
		UserDB: uv,
		pepper: pepper,
	}
}

type userService struct {
	UserDB
	pepper string
}

// Authenticate can be used to authenticate a user with
// the provided email address and password.
func (us *userService) Authenticate(email, password string) (*User, error) {
	foundUser, err := us.ByEmail(email)
	if err != nil {
		return nil, err
	}
	match, err := argon2id.ComparePasswordAndHash(password+us.pepper, foundUser.PasswordHash)
	if err != nil {
		return nil, err
	}
	if !match {
		return nil, errors.ErrPwdIncorrect
	}
	return foundUser, nil
}

type userValFunc func(*User) error

func runUserValFuncs(user *User, fns ...userValFunc) error {
	for _, fn := range fns {
		if err := fn(user); err != nil {
			return err
		}
	}

	return nil
}

func newUserValidator(udb UserDB, hmac hash.HMAC, pepper string) *userValidator {

	return &userValidator{
		UserDB: udb,
		hmac:   hmac,
		emailRegex: regexp.MustCompile(
			`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,16}$`,
		),
		pepper: pepper,
	}

}

var _ UserDB = &userValidator{}

type userValidator struct {
	UserDB
	hmac       hash.HMAC
	emailRegex *regexp.Regexp
	pepper     string
}

// ByID will return ErrNotFound if the given id is zero
// preventing unnecessary database requests
func (uv *userValidator) ByID(id uint) (*User, error) {
	if id == 0 {
		return nil, errors.Wrap(errors.ErrIDInvalid, "error while validating user to be queried by id")
	}

	return uv.UserDB.ByID(id)
}

// ByEmail will normalize the email address before calling
// ByEmail on the UserDB field
func (uv *userValidator) ByEmail(email string) (*User, error) {
	user := User{
		Email: email,
	}

	if err := runUserValFuncs(&user,
		uv.normalizeEmail,
		uv.requireEmail,
		uv.emailFormat,
	); err != nil {
		return nil, errors.Wrap(err, "error while validating user to be queried by email")
	}
	return uv.UserDB.ByEmail(user.Email)
}

// BuRemenber will hash the remember token and then call
// BuRemenber on the subsequent userdb layer
func (uv *userValidator) ByRemember(token string) (*User, error) {
	user := User{
		Remember: token,
	}

	if err := runUserValFuncs(&user,
		uv.hmacRemember,
	); err != nil {
		return nil, errors.Wrap(err, "error while validating token to be queried by remember token")
	}
	return uv.UserDB.ByRemember(user.RememberHash)
}

// Create will create the provided user and backfill data
// like the ID, CreatedAt, and UpdatedAt fiels.
func (uv *userValidator) Create(user *User) error {

	if err := runUserValFuncs(user,
		uv.pwdRequired,
		uv.pwdMinLength,
		uv.argon2Password,
		uv.pwdHashRequired,
		uv.setRememberIfUnset,
		uv.rememberMinBytes,
		uv.hmacRemember,
		uv.rememberHashRequired,
		uv.normalizeEmail,
		uv.requireEmail,
		uv.emailFormat,
		uv.emailIsAvail,
	); err != nil {
		return errors.Wrap(err, "error while validating user to be created")
	}

	return uv.UserDB.Create(user)
}

func (uv *userValidator) AdminUpdateUser(user *User) error {
	if err := runUserValFuncs(
		user,
		uv.validRole,
		uv.idGreaterThanZero,
	); err != nil {
		return errors.Wrap(err,  "error while validating user to be updated")
	}

	return uv.UserDB.AdminUpdateUser(user)
}

func (uv *userValidator) DashboardYield(user *User) ([]YieldPayload, error) {
	if err := runUserValFuncs(user, uv.validRole, uv.isAdmin); err != nil {
		return nil, errors.Wrap(err,  "error while validating user to yield dashboard")
	}

	return uv.UserDB.DashboardYield(user)
}

func (uv *userValidator) UpdateRole(user *User) error {
	if err := runUserValFuncs(user, uv.validRole); err != nil {
		return errors.Wrap(err,  "error while validating user to update user role")
	}
	return uv.UserDB.UpdateRole(user)
}

func (uv *userValidator) UpdateDownRemaining(user *User) error {
	return uv.UserDB.UpdateDownRemaining(user)
}

func (uv *userValidator) isAdmin(user *User) error {
	if user.Role != ADMIN_PERM {
		return errors.ErrNotAdmin
	}
	return nil
}

func (uv *userValidator) validRole(user *User) error {
	_, err := user.Role.GetRole()
	return err
}
func (uv *userValidator) UpdateRememberHash(user *User) error {
	if err := runUserValFuncs(user,
		uv.idGreaterThanZero,
		uv.rememberMinBytes,
		uv.hmacRemember,
		uv.rememberHashRequired,
	); err != nil {
		return errors.Wrap(err, "error while validating user to update user remember hash")
	}

	return uv.UserDB.UpdateRememberHash(user)
}

// Update will update the provided user with all data
// in the provided  user object
func (uv *userValidator) Update(user *User) error {
	if err := runUserValFuncs(user,
		uv.pwdMinLength,
		uv.argon2Password,
		uv.pwdHashRequired,
		uv.rememberMinBytes,
		uv.hmacRemember,
		uv.rememberHashRequired,
		uv.normalizeEmail,
		uv.requireEmail,
		uv.emailFormat,
		uv.emailIsAvail,
	); err != nil {
		return errors.Wrap(err, "error while validating user to be updated")
	}

	return uv.UserDB.Update(user)
}

// Delete will delete the user with the provided id
func (uv *userValidator) Delete(id uint) error {
	user := User{
		Model: Model{
			ID: id,
		},
	}

	if err := runUserValFuncs(&user,
		uv.idGreaterThanZero,
	); err != nil {
		return errors.Wrap(err, "error while validating user to be deleted")
	}

	return uv.UserDB.Delete(id)

}

// argon2Password will hash a user's password with a
// predefined pepper (userPwPepper) and argon2 if the
// password field is not the empty string
func (uv *userValidator) argon2Password(user *User) error {
	if user.Password == "" {
		return nil
	}
	pw := user.Password + uv.pepper
	pwstr, err := argon2id.CreateHash(pw, argon2id.DefaultParams)
	if err != nil {
		return errors.Wrap(err, "error while creating a new argon2 hash")
	}
	user.PasswordHash = pwstr
	user.Password = ""
	return nil
}

func (uv *userValidator) hmacRemember(user *User) error {
	if user.Remember != "" {
		user.RememberHash = uv.hmac.Hash(user.Remember)
	}

	return nil
}

func (uv *userValidator) setRememberIfUnset(user *User) error {
	if user.Remember != "" {
		return nil
	}

	token, err := rand.RememberToken()
	if err != nil {
		return err
	}
	user.Remember = token

	return err
}

func (uv *userValidator) rememberMinBytes(user *User) error {
	if user.Remember == "" {
		return nil
	}

	n, err := rand.NBytes(user.Remember)
	if err != nil {
		return err
	}
	if n < 32 {
		return errors.ErrRememberShort
	}
	return nil
}

func (uv *userValidator) rememberHashRequired(user *User) error {
	if user.RememberHash == "" {
		return errors.ErrRememberInvalid
	}
	return nil
}

func (uv *userValidator) idGreaterThanZero(user *User) error {
	if user.ID <= 0 {
		return errors.ErrIDInvalid
	}
	return nil
}

func (uv *userValidator) normalizeEmail(user *User) error {
	user.Email = strings.ToLower(user.Email)
	user.Email = strings.TrimSpace(user.Email)
	return nil
}

func (uv *userValidator) requireEmail(user *User) error {
	if user.Email == "" {
		return errors.ErrEmailRequired
	}
	return nil
}

func (uv *userValidator) emailFormat(user *User) error {
	if !uv.emailRegex.MatchString(user.Email) {
		return errors.ErrEmailInvalid
	}
	return nil
}

func (uv *userValidator) emailIsAvail(user *User) error {
	existing, err := uv.ByEmail(user.Email)
	if errors.Is(err, errors.ErrNotFound) {
		//Email address is not taken
		return nil
	}

	if err != nil {
		return err
	}

	if user.ID != existing.ID {
		return errors.ErrEmailTaken
	}
	return nil
}

func (uv *userValidator) pwdMinLength(user *User) error {
	if user.Password == "" {
		return nil
	}
	if utf8.RuneCountInString(user.Password) < 8 {
		return errors.ErrPwdShort
	}
	return nil
}

func (uv *userValidator) pwdRequired(user *User) error {
	if user.Password == "" {
		return errors.ErrPwdRequired
	}
	return nil
}

func (uv *userValidator) pwdHashRequired(user *User) error {
	if user.PasswordHash == "" {
		return errors.ErrPwdRequired
	}
	return nil
}

var _ UserDB = &userDB{}

type userDB struct {
	db *sqlx.DB
}

// ByID will look up by the id provided.
// case 1: user, nil
// case 2: nil, ErrNotFound
// case 3: nil, otherError (server error)
func (ud *userDB) ByID(id uint) (*User, error) {
	user := &User{}
	if err := first(ud.db, "users", "id", id, user); err != nil {
		return nil, err
	}
	return user, nil
}

func (ud *userDB) ByEmail(email string) (*User, error) {
	user := &User{}
	if err := first(ud.db, "users", "email", email, user); err != nil {
		return nil, err
	}
	return user, nil
}

// ByRemember looks up a user with the given remember token
// and returns that user. This method expects the remember token token
// to already be hashed
// Errors are the same as ByEmail and ByID
func (ud *userDB) ByRemember(rememberHash string) (*User, error) {
	user := &User{}
	if err := first(ud.db, "users", "remember_hash", rememberHash, user); err != nil {
		return nil, err
	}

	return user, nil
}

// Create will create the provided user and backfill data
// like the ID, CreatedAt, and UpdatedAt fiels.
func (ud *userDB) Create(user *User) error {
	user.Role = BASIC_PERM
	stmt := `INSERT INTO users (created_at,
								updated_at,
								deleted_at,
								name,
								email,
								password_hash,
								remember_hash,
								role)
		    VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`

	_, err := ud.db.Exec(
		stmt,
		time.Now(),
		time.Now(),
		nil,
		user.Name,
		user.Email,
		user.PasswordHash,
		user.RememberHash,
		user.Role,
	)
	if err != nil {
		errors.Wrap(err, "error while creating a new user")
	}
	return nil
}

// Update will update the provided user with all data
// in the provided  user object
func (ud *userDB) Update(user *User) error {
	stmt := `UPDATE users SET updated_at=$1,
							  name=$2,
							  email=$3,
							  password_hash=$4,
							  remember_hash=$5
			WHERE id = $6`

	_, err := ud.db.Exec(stmt, time.Now(), user.Name, user.Email, user.PasswordHash, user.RememberHash, user.ID)
	if err != nil {
		return errors.Wrapf(err, "error while updating user %s in the database", user.ID)
	}
	return nil
}

func (ud *userDB) AdminUpdateUser(user *User) error {

	var deletedAt *time.Time
	tn := time.Now()
	if user.Banned {
		deletedAt = &tn
	}

	stmt := `UPDATE users SET updated_at=$1,
							  deleted_at=$2,
							  role=$3,
							  down_remaining=$4
			WHERE id = $5`

	_, err := ud.db.Exec(stmt, tn, deletedAt, user.Role, user.DownRemaining, user.ID)
	if err != nil {
		errors.Wrapf(err, "error while updating user %d in the database as Admin", user.ID)
	}
	return nil
}

// UpdateRole will update the user role. You must have Admin permissions to do that
func (ud *userDB) UpdateRole(user *User) error {
	stmt := `UPDATE users SET updated_at=$1, role=$2 WHERE id = $3`

	_, err := ud.db.Exec(stmt, time.Now(), user.Role, user.ID)
	if err != nil {
		errors.Wrapf(err, "error while updating role %d from user %d in the database", user.Role, user.ID)
	}
	return nil
}

func (ud *userDB) UpdateRememberHash(user *User) error {
	stmt := `UPDATE users SET updated_at=$1, remember_hash=$2 WHERE id = $3`

	_, err := ud.db.Exec(stmt, time.Now(), user.RememberHash, user.ID)
	if err != nil {
		errors.Wrapf(err, "error while updating remember hash from user %d in the database", user.ID)
	}
	return nil
}

func (ud *userDB) UpdateDownRemaining(user *User) error {
	stmt := `UPDATE users SET updated_at=$1, down_remaining=$2 WHERE id = $3;`

	_, err := ud.db.Exec(stmt, time.Now(), user.DownRemaining, user.ID)
	if err != nil {
		errors.Wrapf(err, "error while updating download remaining to %d from user %d in the database", user.DownRemaining, user.ID)
	}
	return nil
}

// Delete will delete the user with the provided id
func (ud *userDB) Delete(id uint) error {
	_, err := ud.db.Exec(`UPDATE users SET deleted_at=$1, updated_at=$2 WHERE id = $3`, time.Now(), time.Now(), id)
	if err != nil {
		errors.Wrapf(err, "error while deleting user %d in the database", id)
	}
	return nil
}

func (ud *userDB) DashboardYield(user *User) ([]YieldPayload, error) {
	stmt := `SELECT users.id, COUNT(videos.user_id) AS videos, users.deleted_at, users.name AS username, users.email, users.role, users.down_remaining AS remaining FROM users
	LEFT JOIN videos ON users.id = videos.user_id
	GROUP BY users.id ORDER BY id;`

	rows, err := ud.db.Queryx(stmt)
	if err != nil {
		return nil, errors.Wrap(err, "error while selecting dashboard items in the database")
	}
	defer rows.Close()

	var payload []YieldPayload
	for rows.Next() {
		row := make(YieldPayload)
		if err := rows.MapScan(row); err != nil {
			return nil, errors.Wrap(err, "error while scanning row")
		}
		payload = append(payload, row)
	}

	return payload, nil
}

// first will query using the provided DB and it will get
// the first item returned and palt it into dst.
// if nothing is found in the query, it will return
// ErrNotFoun
func first(db *sqlx.DB, table, key string, value, dst interface{}) error {
	err := db.Get(dst, fmt.Sprintf(`SELECT * FROM %s WHERE %s = $1;`, table, key), value)
	if errors.Is(err, sql.ErrNoRows) {
		return errors.Wrapf(errors.ErrNotFound, "value %s where key is %s not found in table %s", value, key, table)
	}
	if err != nil {
		return errors.Wrapf(err, "error while selecting %s where key is %s from table %s", value, key, table)
	}

	if v, ok := dst.(*User); ok {
		if v.Model.DeletedAt != nil {
			return errors.Wrap(errors.ErrBanned, "tried to select a banned user")
		}
	}
	return nil
}
