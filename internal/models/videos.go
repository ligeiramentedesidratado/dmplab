// Package models does ...
package models

import (
	"crypto/md5"
	"strings"
	"time"
	"fmt"
	"unicode/utf8"

	"github.com/jmoiron/sqlx"
	"dmplab/internal/errors"
	"github.com/lib/pq"
)

var runeBlackList = []rune{',', '"', '\\', '/', '|', '{', '}', '!', '=', '<', '>','.','[', ']', '$', '^', '~', '`', ';', '´', ':', '@', '#', '%', '¨', '&', '*', '_', '?', '\t', '\n',  '+', '-', '(', ')'}

// Status Keep track of the current video status
type Status int

const (
	PreCreate = Status(1) << iota
	Create
	URL
	Done
)

// Video TODO: NEEDS COMMENT INFO
type Video struct {
	Model
	Filename    string         `db:"filename" json:"-"`
	Title       string         `db:"title" json:"-"`
	Description string         `db:"description" json:"-"`
	Tags        pq.StringArray `db:"tags" json:"-"`
	// Md5 is calculated in the browser and send through tusd MetaData
	Md5    []byte `db:"md5" json:"-"`
	Size   uint64 `db:"size" json:"-"`
	Status Status `db:"status" json:"-"`
	// TusdID is the name of the file saved in disk
	TusdID        string `db:"tusd_id" json:"-"`
	VideoPubLink  string `db:"video_pub_link" json:"-"`
	ScreenPubLink string `db:"screen_pub_link" json:"-"`
	ThumbPubLink  string `db:"thumb_pub_link" json:"-"`
	UserID        uint   `db:"user_id" json:"-"`
}

// VideoYielder TODO: NEEDS COMMENT INFO
type VideoYielder interface {

	// ByMostRecent will return the 20 most recently created videos
	// or you can specify an offset.
	ByMostRecentYield(offset uint) ([]YieldPayload, error)

	// ByUserIDMostRecent will return the 20 most recently created videos by the given user id
	// or you can specify an offset.
	ByUserIDMostRecentYield(userID, offset uint) ([]YieldPayload, error)

	ByRemberYield(md5 []byte) (YieldPayload, error)
	ByRemberDownloadYield(md5 []byte) (YieldPayload, error)

	// ByTagsYield will return all tags
	TagsYield() ([]YieldPayload, error)

	// ByTagTagsYield will return all videos with the provided tag
	ByTagYield(tag string) ([]YieldPayload, error)
	DashboardYield(user *User) ([]YieldPayload, error)
}

// VideoDB is an interface for the postgres database constraints
// and basic CRUD usage.
// All values returned from methods is meant to be used internally
type VideoDB interface {

	//Query
	// ByUserID returns a video slice with the given user ID.
	ByUserID(userID uint) ([]Video, error)
	// ByID returns the first video structure using the provided id;
	// if the video does not exist, an error `ErrNotFound` will be returned
	ByID(id uint) (*Video, error)
	// ByRember returns the first video structure using the provided md5;
	// if the video does not exist, an error `ErrNotFound` will be returned
	ByRember(md5 []byte) (*Video, error)
	// ByNotDone returns all videos with the status less than Done
	ByNotDone() ([]Video, error)

	//Create
	Create(v *Video) error

	//Update
	Update(v *Video) error
	UpdateStatus(v *Video) error
	UpdateTusdID(v *Video) error
	AdminUpdateVideo(v *Video) error

	//All sophisticated CRUD goes here
	VideoYielder
}

func newVideoService(db *sqlx.DB) VideoService {
	return &videoValidator{
		VideoDB: &videoDB{
			db,
		},
	}
}

type (
	//VideoService will be returned by newVideoService function exposing
	// all methods available.
	VideoService interface {
		VideoDB
	}

	// All runtime validation and normalization needed.
	videoValidator struct {
		VideoDB
	}
)

func (vv *videoValidator) ByUserID(userID uint) ([]Video, error) {
	if userID == 0 {
		return nil, errors.ErrIDInvalid
	}
	return vv.VideoDB.ByUserID(userID)
}

// ByID TODO: NEEDS COMMENT INFO
func (vv *videoValidator) ByID(id uint) (*Video, error) {
	if id == 0 {
		return nil, errors.ErrIDInvalid
	}
	return vv.VideoDB.ByID(id)
}

// ByRember TODO: NEEDS COMMENT INFO
func (vv *videoValidator) ByRember(md5 []byte) (*Video, error) {
	v := &Video{Md5: md5}
	if err := runVideoValFuncs(v,
		vv.rememberHashRequired,
	); err != nil {
		return nil, err
	}

	return vv.VideoDB.ByRember(md5)
}

func (vv *videoValidator) ByNotDone() ([]Video, error) {
	return vv.VideoDB.ByNotDone()
}

func (vv *videoValidator) TagsYield() ([]YieldPayload, error) {
	return vv.VideoDB.TagsYield()
}

func (vv *videoValidator) ByTagYield(tag string) ([]YieldPayload, error) {

	var err error
	if utf8.RuneCountInString(tag) < 2 {
		err = errors.ErrTagTooShort
	}
	if utf8.RuneCountInString(tag) > 16 {
		err = errors.ErrTagTooLong
	}
	if err != nil{
	return nil, errors.Wrap(err, "error while validating tag")
}

	return vv.VideoDB.ByTagYield(tag)
}

// Create TODO: NEEDS COMMENT INFO
func (vv *videoValidator) Create(v *Video) error {
	if err := runVideoValFuncs(v,
		vv.normalizeFileInfo,
		vv.filenameRequired,
		vv.filenameLess128,
		vv.titleMore3,
		vv.titleLess128,
		vv.descLess10240,
		vv.tagsRequired,
		vv.tagMore2,
		vv.tagLess16,
		vv.rememberHashRequired,
		vv.sizeRequired,
		vv.userIDRequired,
	); err != nil {
		return err
	}
	return vv.VideoDB.Create(v)
}

func (vv *videoValidator) UpdateStatus(v *Video) error {
	if len(v.Md5) != md5.Size {
		return errors.ErrRememberInvalid
	}
	return vv.VideoDB.UpdateStatus(v)
}

func (vv *videoValidator) UpadteTusdID(v *Video) error {
	if err := runVideoValFuncs(v,
		vv.requireTusdID,
		vv.videoIDRequired,
	); err != nil {
		return err
	}
	return vv.VideoDB.UpdateTusdID(v)
}

func (vv *videoValidator) requireTusdID(v *Video) error {
	if v.TusdID == "" {
		return errors.ErrTusdIDRequired
	}
	return nil
}

func (vv *videoValidator)AdminUpdateVideo(v *Video) error {

	if err := runVideoValFuncs(v,
		vv.normalizeFileInfo,
		vv.titleMore3,
		vv.titleLess128,
		vv.descLess10240,
		vv.tagsRequired,
		vv.tagMore2,
		vv.tagLess16,
		vv.videoIDRequired,
	); err != nil {
		return err
	}

	return vv.VideoDB.AdminUpdateVideo(v)
}

// ByUserIDMostRecent will validate and/or normalize data.
func (vv *videoValidator) ByUserIDMostRecent(userID, offset uint) ([]YieldPayload, error) {
	if userID == 0 {
		return nil, errors.ErrUserIDRequired
	}

	return vv.VideoDB.ByUserIDMostRecentYield(userID, offset)
}

func (vv *videoValidator) ByRemberYield(md5 []byte) (YieldPayload, error) {
	v := &Video{Md5: md5}
	if err := runVideoValFuncs(v,
		vv.rememberHashRequired,
	); err != nil {
		return nil, err
	}
	return vv.VideoDB.ByRemberYield(md5)
}

func (vv *videoValidator) ByRemberDownloadYield(md5 []byte) (YieldPayload, error) {
	v := &Video{Md5: md5}
	if err := runVideoValFuncs(v,
		vv.rememberHashRequired,
	); err != nil {
		return nil, err
	}
	return vv.VideoDB.ByRemberDownloadYield(md5)
}

func (vv *videoValidator) DashboardYield(user *User) ([]YieldPayload, error) {
	if _, err := user.Role.GetRole(); err != nil {
		return nil, err
	}

	if user.Role != ADMIN_PERM {
		return nil, errors.ErrNotAdmin
	}

	return vv.VideoDB.DashboardYield(user)
}

func (vv *videoValidator) filenameRequired(v *Video) error {
	if v.Filename == "" {
		return errors.ErrFilenameRequired
	}
	return nil
}

func (vv *videoValidator) normalizeFileInfo(v *Video) error {
	v.Title = strings.TrimSpace(v.Title)
	v.Description = strings.TrimSpace(v.Description)

	for idx, tag := range v.Tags {
		tag = strings.ToLower(tag)
		v.Tags[idx] = strings.TrimSpace(tag)
		for _, r := range runeBlackList {
			if strings.ContainsRune(v.Tags[idx], r) {
				return errors.NewModelError(fmt.Sprintf("invalid character: %c", r), errors.Unexpected)
			}

		}
	}

	return nil
}

func (vv *videoValidator) filenameLess128(v *Video) error {

	if utf8.RuneCountInString(v.Filename) >= 128 {
		return errors.ErrFilenameTooLong
	}
	return nil
}

func (vv *videoValidator) titleMore3(v *Video) error {
	if v.Title == "" {
		return errors.ErrTitleTooShort
	}
	return nil
}

func (vv *videoValidator) titleLess128(v *Video) error {

	if utf8.RuneCountInString(v.Title) > 128 {
		return errors.ErrTitleTooLong
	}
	return nil
}

// DescLess512 TODO: NEEDS COMMENT INFO
func (vv *videoValidator) descLess10240(v *Video) error {

	if v.Description == "" {
		return nil
	}

	if utf8.RuneCountInString(v.Description) > 10240 {
		return errors.ErrDescTooLong
	}
	return nil
}

func (vv *videoValidator) tagsRequired(v *Video) error {
	if len(v.Tags) < 3 {
		return errors.ErrTagsTooShort
	}

	if len(v.Tags) > 32 {
		return errors.ErrTagsTooLong
	}

	return nil
}

func (vv *videoValidator) tagMore2(v *Video) error {
	for _, tag := range v.Tags {
		if utf8.RuneCountInString(tag) < 2 {
			return errors.ErrTagTooShort
		}
	}
	return nil
}

func (vv *videoValidator) tagLess16(v *Video) error {
	for _, tag := range v.Tags {
		if utf8.RuneCountInString(tag) > 16 {
			return errors.ErrTagTooLong
		}
	}
	return nil
}

func (vv *videoValidator) rememberHashRequired(v *Video) error {

	if len(v.Md5) != md5.Size {
		return errors.ErrRememberInvalid
	}

	return nil
}

func (vv *videoValidator) sizeRequired(v *Video) error {
	if v.Size == 0 {
		return errors.ErrSizeRequired
	}
	return nil
}

func (vv *videoValidator) userIDRequired(v *Video) error {
	if v.UserID == 0 {
		return errors.ErrIDInvalid
	}
	return nil
}

func (vv *videoValidator) videoIDRequired(v *Video) error {
	if v.ID == 0 {
		return errors.ErrIDInvalid
	}
	return nil
}

type videoValFunc func(*Video) error

func runVideoValFuncs(v *Video, fns ...videoValFunc) error {
	for _, fn := range fns {
		if err := fn(v); err != nil {
			return err
		}
	}
	return nil
}

var _ VideoDB = &videoDB{}

type videoDB struct {
	db *sqlx.DB
}

// ByUserID TODO: NEEDS COMMENT INFO
func (vg *videoDB) ByUserID(userID uint) ([]Video, error) {
	videos := []Video{}
	err := vg.db.Select(&videos,
		"SELECT * FROM videos WHERE videos.deleted_at IS NULL AND user_id = $1 ORDER BY created_at DESC LIMIT 5;",
		userID,
	)
	if err != nil {
		return nil, err
	}

	return videos, nil
}

// ByID TODO: NEEDS COMMENT INFO
func (vg *videoDB) ByID(id uint) (*Video, error) {
	vd := &Video{}
	if err := first(vg.db, "videos", "id", id, vd); err != nil {
		return nil, err
	}
	return vd, nil
}

// ByRember TODO: NEEDS COMMENT INFO
func (vg *videoDB) ByRember(md5 []byte) (*Video, error) {
	vd := &Video{}
	if err := first(vg.db, "videos", "md5", md5, vd); err != nil {
		return nil, err
	}
	return vd, nil
}

// ByRember TODO: NEEDS COMMENT INFO
func (vg *videoDB) ByNotDone() ([]Video, error) {
	videos := []Video{}

	stmt := `SELECT * FROM videos
			WHERE videos.deleted_at IS NULL AND status < $1;`

	if err := vg.db.Select(&videos, stmt, Done,); err != nil {
		return nil, err
	}

	return videos, nil
}

// Create TODO: NEEDS COMMENT INFO
func (vg *videoDB) Create(v *Video) error {
	stmt := `INSERT INTO videos (created_at,
								 updated_at,
								 deleted_at,
								 filename,
								 title,
								 description,
								 tags,
								 md5,
								 size,
								 status,
								 tusd_id,
								 video_pub_link,
								 screen_pub_link,
								 thumb_pub_link,
								 user_id)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)`

	_, err := vg.db.Exec(
		stmt,
		time.Now(),
		time.Now(),
		nil,
		v.Filename,
		v.Title,
		v.Description,
		v.Tags,
		v.Md5,
		v.Size,
		v.Status,
		v.TusdID,
		v.VideoPubLink,
		v.ScreenPubLink,
		v.ThumbPubLink,
		v.UserID,
	)
	return err
}

// Update TODO: NEEDS COMMENT INFO
func (vg *videoDB) Update(v *Video) error {
	stmt := `UPDATE videos SET updated_at=$1,
							   filename=$2,
							   title=$3,
							   description=$4,
							   tags=$5,
							   md5=$6,
							   size=$7,
							   status=$8,
							   tusd_id=$9,
							   video_pub_link=$10,
							   screen_pub_link=$11,
							   thumb_pub_link=$12
			WHERE id = $13`

	_, err := vg.db.Exec(
		stmt,
		time.Now(),
		v.Filename,
		v.Title,
		v.Description,
		v.Tags,
		v.Md5,
		v.Size,
		v.Status,
		v.TusdID,
		v.VideoPubLink,
		v.ScreenPubLink,
		v.ThumbPubLink,
		v.ID,
	)
	return err
}

// UpdateStatus TODO: NEEDS COMMENT INFO
func (vg *videoDB) UpdateStatus(v *Video) error {
	stmt := `UPDATE videos SET status=$1 WHERE id = $2`
	_, err := vg.db.Exec(stmt, v.Status, v.ID)
	return err
}

// UpdateTusdID TODO: NEEDS COMMENT INFO
func (vg *videoDB) UpdateTusdID(v *Video) error {
	stmt := `UPDATE videos SET tusd_id=$1 WHERE id = $2`
	_, err := vg.db.Exec(stmt, v.TusdID, v.ID)
	return err
}

func (vg *videoDB) AdminUpdateVideo(v *Video) error {

	stmt := `UPDATE videos SET updated_at=$1,
								deleted_at=$2,
								title=$3,
								description=$4,
								tags=$5
								WHERE id = $6`

	_, err := vg.db.Exec(stmt, time.Now(), v.DeletedAt, v.Title, v.Description, v.Tags, v.ID)
	return err
}

// ByMostRecent will return 20 most recent videos
func (vg *videoDB) ByMostRecentYield(offset uint) ([]YieldPayload, error) {

	stmt := `SELECT videos.created_at AS created, encode(videos.md5, 'hex') AS md5, videos.title, videos.size, videos.thumb_pub_link, users.name AS username
			FROM videos
			JOIN users ON videos.user_id = users.id
			WHERE status >= $1
			AND videos.deleted_at IS NULL
			ORDER BY created DESC
			OFFSET $2
			LIMIT 5;`

	rows, err := vg.db.Queryx(stmt, Done, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var payload []YieldPayload
	for rows.Next() {
		row := make(YieldPayload)
		if err := rows.MapScan(row); err != nil {
			return nil, err
		}
		payload = append(payload, row)
	}

	return payload, nil
}

// ByUserIDMostRecent will return 20 most recent videos by the given user id
func (vg *videoDB) ByUserIDMostRecentYield(userID, offset uint) ([]YieldPayload, error) {

	stmt := `SELECT videos.created_at AS created, encode(videos.md5, 'hex') AS md5, videos.title, videos.size, videos.thumb_pub_link, videos.screen_pub_link, videos.filename, videos.tags, videos.description AS desc, videos.user_id, users.name AS username
			FROM videos
			JOIN users ON videos.user_id = users.id
			WHERE status >= $1
			AND user_id = $2
			AND videos.deleted_at IS NULL
			ORDER BY created
			OFFSET $3
			LIMIT 20;`

	rows, err := vg.db.Queryx(stmt, Done, userID, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var payload []YieldPayload
	for rows.Next() {
		row := make(YieldPayload)
		if err := rows.MapScan(row); err != nil {
			return nil, err
		}

		row.ParseArray("tags")

		payload = append(payload, row)
	}

	return payload, nil
}

func (vg *videoDB) ByRemberYield(md5 []byte) (YieldPayload, error) {

	stmt := `SELECT videos.created_at AS created, encode(videos.md5, 'hex') AS md5, videos.title, videos.size, videos.thumb_pub_link, videos.screen_pub_link, videos.filename, videos.tags, videos.description AS desc, videos.user_id, users.name AS username
			FROM videos
			JOIN users ON videos.user_id = users.id
			WHERE status >= $1
			AND md5 = $2
			AND videos.deleted_at IS NULL
			LIMIT 1;`

	payload := make(YieldPayload)
	if err := vg.db.QueryRowx(stmt, Done, md5).MapScan(payload); err != nil {
		return nil, err
	}
	payload.ParseArray("tags")
	return payload, nil
}

func (vg *videoDB) ByRemberDownloadYield(md5 []byte) (YieldPayload, error) {

	stmt := `SELECT video_pub_link FROM videos
			WHERE status >= $1
			AND md5 = $2
			AND deleted_at IS NULL
			LIMIT 1;`

	payload := make(YieldPayload)
	if err := vg.db.QueryRowx(stmt, Done, md5).MapScan(payload); err != nil {
		return nil, err
	}
	return payload, nil
}

func (vg *videoDB) DashboardYield(user *User) ([]YieldPayload, error) {

	stmt := `SELECT id,
					created_at,
					updated_at,
					deleted_at,
					filename,
					title,
					description,
					tags,
					encode(md5, 'hex') AS md5,
					size,
					status,
					tusd_id,
					video_pub_link,
					screen_pub_link,
					thumb_pub_link,
					user_id
			FROM videos;
					`
	rows, err := vg.db.Queryx(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var payload []YieldPayload
	for rows.Next() {
		row := make(YieldPayload)
		if err := rows.MapScan(row); err != nil {
			return nil, err
		}

		row.ParseArray("tags")

		payload = append(payload, row)
	}

	return payload, nil
}
func (vg *videoDB) TagsYield() ([]YieldPayload, error) {
	stmt := `SELECT UNNEST(tags) AS t,
					COUNT(*) AS c
					FROM videos
					GROUP BY t
					ORDER BY t ASC;`

	rows, err := vg.db.Queryx(stmt)
	if err != nil {
		return nil, errors.Wrap(err, "error while selecting all tags in the database")
	}
	defer rows.Close()

	var tags []YieldPayload
	for rows.Next() {
		row := make(YieldPayload)
		if err := rows.MapScan(row); err != nil {
			return nil, errors.Wrap(err, "error while scanning row")
		}
		tags = append(tags, row)
	}

	return tags, nil
}

func (vg *videoDB) ByTagYield(tag string) ([]YieldPayload, error) {
	stmt := `SELECT videos.created_at AS created, encode(videos.md5, 'hex') AS md5, videos.title, videos.size, videos.thumb_pub_link, users.name AS username
			FROM videos
			JOIN users ON videos.user_id = users.id
			WHERE status >= $1
			AND $2 = ANY(tags)
			AND videos.deleted_at IS NULL
			ORDER BY created DESC;`

	rows, err := vg.db.Queryx(stmt, Done, tag)
	if err != nil {
		return nil, errors.Wrap(err, "error while selecting videos by tag in the database")
	}
	defer rows.Close()

	var payload []YieldPayload
	for rows.Next() {
		row := make(YieldPayload)
		if err := rows.MapScan(row); err != nil {
			return nil, errors.Wrap(err, "error while scanning row")
		}
		payload = append(payload, row)
	}

	return payload, nil
}
