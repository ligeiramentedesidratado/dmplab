package views

import (
	"dmplab/pkg/context"
	"net/http"

	"github.com/go-chi/render"
)

type API struct{}

func (v *API) Render(w http.ResponseWriter, r *http.Request, data interface{}) error {

	var vd Data
	switch d := data.(type) {
	case Data:
		vd = d
		//Do nothing
	default:
		vd = Data{
			Yield: data,
		}
	}

	if vd.User == nil {
		user := context.User(r.Context())
		vd.User = user
	}

	if vd.Status > 0 {
		render.Status(r, vd.Status)
	}

	if err := render.Render(w, r, &vd); err != nil {
		return err
	}

	return nil
}
