package controllers

import (
	"dmplab/internal/models"
	"dmplab/internal/errors"
	"dmplab/pkg/context"
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/gorilla/schema"
)

type SignupForm struct {
	Name     string `schema:"name" json:"name"`
	Email    string `schema:"email" json:"email"`
	Password string `schema:"password" json:"password"`
}

type LoginForm struct {
	Email    string `schema:"email" json:"email"`
	Password string `schema:"password" json:"password"`
}

func parseForm(r *http.Request, dst interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}
	return parserValues(r.PostForm, dst)
}

func parseURLParams(r *http.Request, dst interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}

	return parserValues(r.Form, dst)
}

func parserValues(values url.Values, dst interface{}) error {
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	if err := dec.Decode(dst, values); err != nil {
		return err
	}

	return nil
}

func decodeJSONBody(r *http.Request, dst interface{}) error {
	return json.NewDecoder(r.Body).Decode(dst)
}

func getUserWithCtx(r *http.Request) (*models.User, error) {
	user := context.User(r.Context())
	if user == nil {
		return nil, errors.ErrUserNil
	}
	return user, nil
}

func getVideoWithCtx(r *http.Request) (*models.Video, error) {
	video := context.Video(r.Context())
	if video == nil {
		return nil, errors.ErrVideoNil
	}
	return video, nil
}

func getURLID(r *http.Request, urlParam string) (uint, error) {
	return toUint(chi.URLParam(r, urlParam))
}

func toUint(s string) (uint, error) {

	value, err := strconv.ParseUint(s, 10, 32)
	if err != nil {
		return 0, errors.ErrIDInvalid
	}
	return uint(value), nil
}
