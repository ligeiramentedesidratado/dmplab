package controllers

import (
	"dmplab/internal/models"
)

type ControllerServicesConf func(*ControllerServices) error

func WithUserAPICtr(us models.UserService) ControllerServicesConf {
	return func(c *ControllerServices) error {
		c.UsersAPI = NewUserAPI(us)
		return nil
	}
}

func WithVideoAPICtr(vs models.VideoService, us models.UserService) ControllerServicesConf {
	return func(c *ControllerServices) error {
		c.VideoAPI = NewVideoAPI(vs, us)
		return nil
	}
}

func WithAboutAPICtr(as models.AboutService) ControllerServicesConf {
	return func(c *ControllerServices) error {
		c.AboutAPI = NewAboutAPI(as)
		return nil
	}
}

func NewControllerServices(cfgFns ...ControllerServicesConf) (*ControllerServices, error) {
	var Ctrls ControllerServices
	for _, cf := range cfgFns {
		if err := cf(&Ctrls); err != nil {
			return nil, err
		}
	}

	return &Ctrls, nil
}

type ControllerServices struct {
	UsersAPI *UsersAPI
	VideoAPI *VideoAPI
	AboutAPI *AboutAPI
}
