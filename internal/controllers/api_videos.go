package controllers

import (
	"dmplab/internal/models"
	"dmplab/internal/views"
	"encoding/hex"
	"net/http"
	"time"

	"github.com/go-chi/chi"
)

type VideoAPI struct {
	VideoViewAPI *views.API
	vs           models.VideoService
	us           models.UserService
}

func NewVideoAPI(vs models.VideoService, us models.UserService) *VideoAPI {
	return &VideoAPI{
		VideoViewAPI: &views.API{},
		vs:           vs,
		us:           us,
	}
}

// GetVideo will respond with Video resource
// GET /videos/:id
func (va *VideoAPI) GetVideo(w http.ResponseWriter, r *http.Request) {
	var vd views.Data

	bs, err := hex.DecodeString(chi.URLParam(r, "videoID"))
	if err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	video, err := va.vs.ByRemberYield(bs)
	if err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = video

	va.VideoViewAPI.Render(w, r, vd)

}

// GetVideos will respond with Video collection.
// Specifying an invalid `offset` parameter will not respond with an error, it will assume 0 instead
// GET /videos
func (va *VideoAPI) GetVideos(w http.ResponseWriter, r *http.Request) {
	var vd views.Data

	//we can ignore errors here because the value will be set to zero by default
	offset, _ := toUint(r.URL.Query().Get("offset"))

	videos, err := va.vs.ByMostRecentYield(offset)
	if err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = videos
	va.VideoViewAPI.Render(w, r, vd)

}

// GetUserVideos will respond with Video collection.
// You must provide `userid` url parameter in order to
// get all videos from that user.
func (va *VideoAPI) GetUserVideos(w http.ResponseWriter, r *http.Request) {
	var vd views.Data

	//we can ignore errors here because the value will be set to zero by default
	offset, _ := toUint(r.URL.Query().Get("offset"))

	userid, err := toUint(r.URL.Query().Get("userid"))
	if err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	videos, err := va.vs.ByUserIDMostRecentYield(userid, offset)
	if err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = videos
	va.VideoViewAPI.Render(w, r, vd)

}

func (va *VideoAPI) GetVideoDownloadURL(w http.ResponseWriter, r *http.Request) {

	var vd views.Data

	user, err := getUserWithCtx(r)
	if err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	if user.DownRemaining == 0 {
		vd.AlertError("You have reached your daily limite. Next reset is 4:20 AM.")
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	bs, err := hex.DecodeString(chi.URLParam(r, "videoID"))
	if err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	url, err := va.vs.ByRemberDownloadYield(bs)
	if err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	user.DownRemaining -= 1

	if err := va.us.UpdateDownRemaining(user); err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = url

	va.VideoViewAPI.Render(w, r, vd)
}

func (va *VideoAPI) VideoDashboard(w http.ResponseWriter, r *http.Request) {
	var vd views.Data

	user, err := getUserWithCtx(r)
	if err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusUnauthorized)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	payload, err := va.vs.DashboardYield(user)
	if err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusInternalServerError)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = payload

	va.VideoViewAPI.Render(w, r, vd)
}

// CreateVideo will create a new resource in the database
// POST /videos
func (va *VideoAPI) CreateVideo(w http.ResponseWriter, r *http.Request) {

}

// UpdateVideo will update the resource with the given id
// PUT /videos/:id
func (va *VideoAPI) UpdateVideo(w http.ResponseWriter, r *http.Request) {

}

// AdminUpdateVideo will update the resource with the given id
// ADMIN PERM ONLY
// PUT /dashboard/video
func (va *VideoAPI) AdminUpdateVideo(w http.ResponseWriter, r *http.Request) {

	var vd views.Data

	tempVideo := struct {
		ID          uint     `json:"id"`
		Title       string   `json:"title"`
		Description string   `json:"description"`
		Tags        []string `json:"tags"`
		Deleted     bool     `json:"deleted"`
	}{}

	if err := decodeJSONBody(r, &tempVideo); err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusInternalServerError)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	video := models.Video{
		Model:       models.Model{ID: tempVideo.ID},
		Title:       tempVideo.Title,
		Description: tempVideo.Description,
		Tags:        tempVideo.Tags,
	}

	if tempVideo.Deleted {
		tn := time.Now()
		video.DeletedAt = &tn
	}

	if err := va.vs.AdminUpdateVideo(&video); err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	va.VideoViewAPI.Render(w, r, vd)
}

// DeleteVideo will delete a resource in the database if it exists
// DELETE /videos/:id
func (va *VideoAPI) DeleteVideo(w http.ResponseWriter, r *http.Request) {

}

func (va *VideoAPI) GetAllTags(w http.ResponseWriter, r *http.Request) {

	var vd views.Data

	tags, err := va.vs.TagsYield()
	if err != nil{
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = tags

	va.VideoViewAPI.Render(w, r, vd)
}

func (va *VideoAPI) GetVideosByTag(w http.ResponseWriter, r *http.Request) {

	var vd views.Data

	tag := chi.URLParam(r, "tag")

	videos, err := va.vs.ByTagYield(tag)
	if err != nil {
		vd.SetAlert(err)
		va.VideoViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = videos
	va.VideoViewAPI.Render(w, r, vd)
}
