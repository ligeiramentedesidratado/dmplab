package controllers

import (
	"dmplab/internal/errors"
	"dmplab/internal/models"
	"dmplab/internal/proto/clients/config"
	"dmplab/internal/proto/clients/log"
	"dmplab/internal/views"
	"dmplab/pkg/rand"
	"net/http"
	"time"
)

type UsersAPI struct {
	UsersViewAPI *views.API
	us           models.UserService
}

func NewUserAPI(us models.UserService) *UsersAPI {
	return &UsersAPI{
		UsersViewAPI: &views.API{},
		us:           us,
	}
}

// Login ...
// POST /login
func (ua *UsersAPI) Login(w http.ResponseWriter, r *http.Request) {

	// Haha stick this captch in your ass
	time.Sleep(time.Second)

	var vd views.Data
	var bodyPayload LoginForm

	if err := decodeJSONBody(r, &bodyPayload); err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusBadRequest)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	user, err := ua.us.Authenticate(bodyPayload.Email, bodyPayload.Password)
	if err != nil {
		switch {
		case errors.Is(err, errors.ErrNotFound):
			vd.AlertError("Invalid email address.")
		default:
			vd.SetAlert(err)
		}

		vd.SetStatus(http.StatusBadRequest)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	if err := ua.signIn(w, user); err != nil {
		vd.SetAlert(err)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	vd.User = user
	vd.AlertSuccess("Welcome Back!")
	ua.UsersViewAPI.Render(w, r, vd)
}

// Register ...
// POST /register
func (ua *UsersAPI) Register(w http.ResponseWriter, r *http.Request) {

	// Haha stick this captch in your ass
	time.Sleep(time.Second)

	var vd views.Data
	var bodyPayload SignupForm

	if err := decodeJSONBody(r, &bodyPayload); err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusBadRequest)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	user := &models.User{
		Name:     bodyPayload.Name,
		Email:    bodyPayload.Email,
		Password: bodyPayload.Password,
	}

	if err := ua.us.Create(user); err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusBadRequest)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	if err := ua.signIn(w, user); err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusUnauthorized)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	vd.User = user
	log.Infof(0, "a new user (%s) registered", user.Name)
	vd.AlertSuccess("Welcome to DMP!")
	ua.UsersViewAPI.Render(w, r, vd)
}

func (ua *UsersAPI) Logout(w http.ResponseWriter, r *http.Request) {

	cookie := http.Cookie{
		Name:     "dmp_remember_token",
		Value:    "",
		Expires:  time.Now(),
		HttpOnly: true,
		Secure:   true,
	}

	http.SetCookie(w, &cookie)

	var vd views.Data
	user, err := getUserWithCtx(r)
	if err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusUnauthorized)
		ua.UsersViewAPI.Render(w, r, vd)
	}
	token, err := rand.RememberToken()
	if err != nil {
		token = "temp"
		log.Errorf(0, "couldn't generate a token: %v", err)
	}
	user.Remember = token

	if err := ua.us.UpdateRememberHash(user); err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusInternalServerError)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	ua.UsersViewAPI.Render(w, r, vd)
}

// signIn is used to signIn the given user in via cookies
func (ua *UsersAPI) signIn(w http.ResponseWriter, user *models.User) error {

	if user.Remember == "" {
		token, err := rand.RememberToken()
		if err != nil {
			return errors.Wrap(err, "error while generating a new remember token")
		}
		user.Remember = token
		err = ua.us.Update(user)
		if err != nil {
			return errors.Wrap(err, "error while updating user remember token")
		}
	}

	cookie := http.Cookie{
		Name:     "dmp_remember_token",
		Value:    user.Remember,
		Expires:  time.Now().Add(time.Hour * 3),
		Domain:   config.GetInitcfg().GetAddr(),
		Path:     "/",
		HttpOnly: true,
		Secure:   true,
	}

	http.SetCookie(w, &cookie)

	log.Info(int(user.ID), "logged in")
	return nil
}

func (ua *UsersAPI) GetDecryptKeys(w http.ResponseWriter, r *http.Request) {
	var vd views.Data

	keys := config.GetRcloneKeys()

	if keys.Pass == "" || keys.Salt == "" {
		vd.SetAlert(errors.NewPriveteError("Blank keys"))
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = keys
	ua.UsersViewAPI.Render(w, r, vd)
}

func (ua *UsersAPI) UserDashboard(w http.ResponseWriter, r *http.Request) {
	var vd views.Data

	user, err := getUserWithCtx(r)
	if err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusUnauthorized)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	payload, err := ua.us.DashboardYield(user)
	if err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusInternalServerError)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = payload

	ua.UsersViewAPI.Render(w, r, vd)
}

func (ua *UsersAPI) UpdateUser(w http.ResponseWriter, r *http.Request) {

	var vd views.Data
	var user models.User

	if err := decodeJSONBody(r, &user); err != nil {
		vd.SetAlert(err)
		vd.SetStatus(http.StatusBadRequest)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	if err := ua.us.AdminUpdateUser(&user); err != nil {
		vd.SetAlert(err)
		ua.UsersViewAPI.Render(w, r, vd)
		return
	}

	ua.UsersViewAPI.Render(w, r, vd)
}
