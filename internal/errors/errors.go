package errors

import (
	"errors"
	"fmt"
	"net/http"

	"google.golang.org/grpc/codes"
)

type kind int

const (
	Unexpected = kind(iota)
	Internal
	PermissionDenied
	Unauthenticated
	AlreadyExists
)

func (k kind) ToGRPC() codes.Code {
	switch k {
	case Unexpected:
		return codes.Unknown
	case AlreadyExists:
		return codes.AlreadyExists
	case PermissionDenied:
		return codes.PermissionDenied
	case Unauthenticated:
		return codes.Unauthenticated
	case Internal:
		fallthrough
	default:
		return codes.Internal
	}
}

func (k kind) ToHTTP() int {
	switch k {
	case Unexpected:
		return http.StatusBadRequest
	case AlreadyExists:
		return http.StatusConflict
	case PermissionDenied:
		return http.StatusForbidden
	case Unauthenticated:
		return http.StatusUnauthorized
	case Internal:
		fallthrough
	default:
		return http.StatusInternalServerError
	}
}

type ModelError struct {
	Err    error
	Pkg    string
	Status kind
}

type PrivateError struct {
	Err    error
	Pkg    string
	Status kind
}

var GenericError = ModelError{
	Err:    errors.New("something went wrong. If the problem persists, please email formydemons@pm.me"),
	Pkg:    "models",
	Status: Internal,
}

// Sentinel errors declaration
var (
	ErrIDInvalid       = NewPriveteError("ID must be a number and greater than 0")
	ErrRememberShort   = NewPriveteError("remember token must be at least 32 bytes")
	ErrRememberInvalid = NewPriveteError("remember hash is invalid or empty")
	ErrUserIDRequired  = NewPriveteError("user ID is required")
	ErrUserNil         = NewPriveteError("user context returned nil value")
	ErrVideoNil        = NewPriveteError("video context returned nil value")
	ErrGalleryIDNil    = NewPriveteError("gallery id context returned nil value")
	ErrProfileIDNil    = NewPriveteError("profile id context returned nil value")
	ErrFileIDNil       = NewPriveteError("file id context returned nil value")
	ErrDoneTrue        = NewPriveteError("you cannot create a file with Done == true")
	ErrMD5Invalid      = NewPriveteError("MD5 was not set or is invalid")
	ErrTusdIDRequired  = NewPriveteError("invalid TusdID filename")
	ErrNotDone         = NewPriveteError("the file status is not done")
	ErrRoleInvalid     = NewPriveteError("invalid role")
	ErrStatusInvalid   = NewPriveteError("invalid status")

	// Public errors
	ErrEmailInvalid     = NewModelError("email address is not valid", Unauthenticated)
	ErrEmailTaken       = NewModelError("email address is already taken", Unauthenticated)
	ErrEmailRequired    = NewModelError("email address is required", Unauthenticated)
	ErrPwdIncorrect     = NewModelError("incorrect password provided", Unauthenticated)
	ErrPwdShort         = NewModelError("password must be at least 8 characters long", Unauthenticated)
	ErrPwdRequired      = NewModelError("password is required", Unauthenticated)
	ErrTagsTooShort     = NewModelError("3 tags at least", Unexpected)
	ErrTagsTooLong      = NewModelError("32 tags is the maximum", Unexpected)
	ErrTagTooShort      = NewModelError("tag must have 2 or more characters", Unexpected)
	ErrTagTooLong       = NewModelError("tag must be less than 16 characters", Unexpected)
	ErrFilenameRequired = NewModelError("file name required", Unexpected)
	ErrFilenameTooLong  = NewModelError("filename must be less than 128 characters", Unexpected)
	ErrTitleTooShort    = NewModelError("title must have 3 or more characters", Unexpected)
	ErrTitleTooLong     = NewModelError("title must be less than 128 characters", Unexpected)
	ErrDescTooLong      = NewModelError("description must be less than 10240 characters", Unexpected)
	ErrSizeRequired     = NewModelError("file size must be greater than 0", Unexpected)
	ErrNotFound         = NewModelError("resource not found", Unexpected)
	ErrNotAdmin         = NewModelError("admin perm required", Unexpected)
	ErrAboutDescTooLong = NewModelError("description must be less than 10240 characters", Unexpected)
	ErrAboutCatTooLong  = NewModelError("category must be less than 64 characters", Unexpected)
	ErrThumbRequired    = NewModelError("invalid thumbnail", Unexpected)
	ErrBanned           = NewModelError("this account has been banned", Unexpected)
)

// NewPriveteError will retun a PrivateError data struct error with `Internal Server Error`
// status as default. This error is meant to used internally
func NewPriveteError(err string) error {
	return &PrivateError{
		Err:    errors.New(err),
		Pkg:    "models",
		Status: Internal,
	}
}
func (e PrivateError) Error() string {
	return fmt.Sprintf("%s: %v", e.Pkg, e.Err.Error())
}

func NewModelError(err string, statusCode kind) error {
	return &ModelError{
		Err:    errors.New(err),
		Pkg:    "models",
		Status: statusCode,
	}
}

func (e ModelError) Error() string {
	return fmt.Sprintf("%s: %v", e.Pkg, e.Err.Error())
}

func (e ModelError) Public() string {
	return e.Err.Error()
}

func GetError(err error) string {
	var pub PublicError
	if errors.As(err, &pub) {
		return pub.Public()
	}

	return GenericError.Error()
}

func Wrap(err error, message string) error {
	return fmt.Errorf("%s: %w", message, err)
}

func Wrapf(err error, format string, args ...interface{}) error {
	message := fmt.Sprintf(format, args...)
	return fmt.Errorf("%s: %w", message, err)
}

// Is is a wrap around Is func of the errors' standard library
func Is(err error, target error) bool {
	return errors.Is(err, target)
}

// As is a wrap around As func of the errors' standard library
func As(err error, target interface{}) bool {
	return errors.As(err, target)
}

type PublicError interface {
	error
	Public() string
}
