enum PERM {
  READ = 1 << 0,
  READ_ANY = 1 << 1,
  CREATE = 1 << 2,
  EDIT_OWN = 1 << 3,
  DELETE_OWN = 1 << 4,
  EDIT_ANY = 1 << 5,
  DELETE_ANY = 1 << 6
}

enum ROLE {
  BASIC_PERM = PERM.READ,
  UPLOADER_PERM = PERM.READ | PERM.CREATE | PERM.EDIT_OWN | PERM.DELETE_OWN,
  VIP_PERM = PERM.READ | PERM.READ_ANY,
  VIP_UPLOADER_PERM = PERM.READ |
    PERM.READ_ANY |
    PERM.CREATE |
    PERM.EDIT_OWN |
    PERM.DELETE_OWN,
  MODERATOR_PERM = PERM.READ | PERM.EDIT_ANY,
  ADMIN_PERM = PERM.READ |
    PERM.READ_ANY |
    PERM.CREATE |
    PERM.EDIT_OWN |
    PERM.DELETE_OWN |
    PERM.EDIT_ANY |
    PERM.DELETE_ANY
}

class Role {
  private _role: ROLE;

  constructor(value: number) {
    this.roleSetter(value);
  }

  private roleSetter(value: number) {
    if (ROLE[value]) {
      this._role = value;
    } else {
      console.log("invalid value: ", value);
      this._role = 1;
    }
  }

  get CanRead(): boolean {
    return (this._role & PERM.READ) != 0;
  }
  get CanReadAny(): boolean {
    return (this._role & PERM.READ_ANY) != 0;
  }
  get CanCreate(): boolean {
    return (this._role & PERM.CREATE) != 0;
  }
  get CanEdit(): boolean {
    return (this._role & PERM.EDIT_OWN) != 0;
  }
  get CanEditAny(): boolean {
    return (this._role & PERM.EDIT_ANY) != 0;
  }
  get CanDelete(): boolean {
    return (this._role & PERM.DELETE_OWN) != 0;
  }
  get CanDeleteAny(): boolean {
    return (this._role & PERM.DELETE_ANY) != 0;
  }
  get GetRole(): ROLE {
    return this._role;
  }
  get GetRoleStr(): number | string {
    return ROLE[this._role];
  }
  set GetRoleStr(value) {
    if ("string" === typeof value) {
      value = parseInt(value, 10);
    }
    this.roleSetter(value);
  }
}

const GetAllRoles = () => {
  return [
    { text: "BASIC_PERM", value: ROLE.BASIC_PERM },
    { text: "UPLOADER_PERM", value: ROLE.UPLOADER_PERM },
    { text: "VIP_PERM", value: ROLE.VIP_PERM },
    { text: "VIP_UPLOADER_PERM", value: ROLE.VIP_UPLOADER_PERM },
    { text: "MODERATOR_PERM", value: ROLE.MODERATOR_PERM },
    { text: "ADMIN_PERM", value: ROLE.ADMIN_PERM }
  ];
};

export { Role, ROLE, GetAllRoles };
