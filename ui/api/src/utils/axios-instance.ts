import axios from "axios";

const instance = axios.create({
  baseURL: "https://api." + window.location.hostname + "/v1",
  withCredentials: true
});

export default instance;
