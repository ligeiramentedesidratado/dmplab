/**
 * @fileoverview gRPC-Web generated client stub for logger
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import {
  Message,
  Void} from './logger_pb';

export class LoggerServiceClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: string; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: string; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfoSendLog = new grpcWeb.AbstractClientBase.MethodInfo(
    Void,
    (request: Message) => {
      return request.serializeBinary();
    },
    Void.deserializeBinary
  );

  sendLog(
    request: Message,
    metadata: grpcWeb.Metadata | null): Promise<Void>;

  sendLog(
    request: Message,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: Void) => void): grpcWeb.ClientReadableStream<Void>;

  sendLog(
    request: Message,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: Void) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/logger.LoggerService/SendLog',
        request,
        metadata || {},
        this.methodInfoSendLog,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/logger.LoggerService/SendLog',
    request,
    metadata || {},
    this.methodInfoSendLog);
  }

  methodInfoStreamLog = new grpcWeb.AbstractClientBase.MethodInfo(
    Message,
    (request: Void) => {
      return request.serializeBinary();
    },
    Message.deserializeBinary
  );

  streamLog(
    request: Void,
    metadata?: grpcWeb.Metadata) {
    return this.client_.serverStreaming(
      this.hostname_ +
        '/logger.LoggerService/StreamLog',
      request,
      metadata || {},
      this.methodInfoStreamLog);
  }

}

