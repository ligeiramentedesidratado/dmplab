import { RootState, FileState } from "@/types";
import { MutationTree, ActionTree, GetterTree } from "vuex";

const state: FileState = {};

const mutations: MutationTree<FileState> = {
  // 'SET_FILE'(state, payload) {
  // },
};

const actions: ActionTree<FileState, RootState> = {};

const getters: GetterTree<FileState, RootState> = {};

const module = {
  state,
  mutations,
  actions,
  getters
};

export default module;
