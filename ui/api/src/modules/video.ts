import { VideoState, RootState, MediaVideo } from "@/types";
import axios from "../utils/axios-instance";
import { MutationTree, ActionTree, GetterTree } from "vuex";

const state: VideoState = {
  videos: [] as Array<MediaVideo>
};

const mutations: MutationTree<VideoState> = {
  SET_VIDEO(state, video: MediaVideo) {
    state.videos.push(video);
  },
  CLEAR_VIDEOS(state) {
    state.videos = [];
  }
};

const actions: ActionTree<VideoState, RootState> = {
  getLatestVideos({ commit, state }, { vm, offset }) {
    if (offset === undefined || offset < 0) {
      offset = 0;
    }
    if (state.videos.length > 0 && offset === 0) return;

    axios
      .get("/videos", { params: { offset: offset } })
      .then(resp => {
        const data: Array<MediaVideo> = resp.data.yield;
        if (data === null) return;

        data.forEach((item, _) => {
          this.dispatch("blobToImg", item.thumb_pub_link)
            .then(blob => {
              vm.$set(item, "blob_thumb_link", blob);
            })
            .catch(_ => {
              vm.$set(item, "blob_thumb_link", "/assets/err.jpeg");
            });
          commit("SET_VIDEO", item);
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
};

const getters: GetterTree<VideoState, RootState> = {
  getVideos: (state): Array<MediaVideo> => {
    return state.videos;
  }
};

const module = {
  state,
  mutations,
  actions,
  getters
};

export default module;
