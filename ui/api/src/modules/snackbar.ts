import { SnackState, StatusType, Status, RootState } from "@/types";
import { Module, ActionTree, MutationTree, GetterTree } from "vuex";
import { mdiExclamationThick, mdiAlert, mdiCheck } from "@mdi/js";

const state: SnackState = {
  status: { text: "", icon: "", type: StatusType.success },
  show: false
};

const mutations: MutationTree<SnackState> = {
  SET_SNACK_TEXT(state, status: Status) {
    status.icon =
      status.type === StatusType.success
        ? mdiCheck
        : status.type === StatusType.warning
        ? mdiAlert
        : mdiExclamationThick;
    state.status = status;
  },
  SET_SNACK_SHOW(state, show) {
    state.show = show;
  }
};

const actions: ActionTree<SnackState, RootState> = {
  snackbar({ commit }, payload: Status) {
    commit("SET_SNACK_TEXT", payload);
    commit("SET_SNACK_SHOW", true);
  },

  snackbarGenericError({ commit }) {
    const errNotify = {
      text: "Something went wrong. If the problem persists, please email formydemons@pm.me.",
      type: StatusType.error
    };

    commit("SET_SNACK_TEXT", errNotify);
    commit("SET_SNACK_SHOW", true);
  },

  closeSnack({ commit }) {
    commit("SET_SNACK_SHOW", false);
  }
};

const getters: GetterTree<SnackState, RootState> = {
  showSanck: state => {
    return state;
  }
};

const module: Module<SnackState, RootState> = {
  state,
  mutations,
  actions,
  getters
};

export default module;
