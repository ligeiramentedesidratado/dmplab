import { DecryptState, RootState } from "@/types";
import toBlobURL from "stream-to-blob-url";
import Decryptor from "../utils/decryptor";

import { ActionTree } from "vuex";

const actions: ActionTree<DecryptState, RootState> = {
  async blobToImg(_, fetchURL: string) {
    return new Promise((resolve, reject) => {
      if (fetchURL === undefined || fetchURL == "") {
        reject(new Error("empty URL"));
        return;
      }

      const decryptedStream = Decryptor.rcRef().loadImage(fetchURL);

      toBlobURL(decryptedStream(), "image/jpeg")
        .then((blobf: any) => {
          resolve(blobf);
        })
        .catch((err: Error) => {
          reject(err);
        });
    });
  },

  async blobToDownload(_, { fetchURL, filename }) {
    return new Promise((resolve, reject) => {
      if (fetchURL === undefined || fetchURL == "") {
        reject(new Error("empty URL"));
        return;
      }

      const decryptedStream = Decryptor.rcRef().downloadFile(
        fetchURL,
        filename,
        "/assets/mitm.html"
      );
      resolve(decryptedStream());
    });
  }
};

const module = {
  actions
};

export default module;
