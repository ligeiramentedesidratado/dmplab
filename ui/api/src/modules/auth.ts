import axios from "../utils/axios-instance";
import { ROLE, Role } from "@/utils/role";
import router from "@/router";
import Decryptor from "../utils/decryptor";
import { AuthState, RootState, User, Status } from "@/types";
import { MutationTree, ActionTree, GetterTree } from "vuex";

const state: AuthState = {
  user: {
    id: 0,
    name: "",
    email: "",
    created: "",
    role: new Role(1),
    remaining: 0
  } as User,

  isAuth: false
};

const mutations: MutationTree<AuthState> = {
  SET_AUTH(state, auth: boolean) {
    auth ? Decryptor.rcInit() : Decryptor.rcDestroy();
    state.isAuth = auth;
  },

  SET_RAMAINING(state, remain: number) {
    state.user.remaining = remain;
  },

  SET_USER(state, payload: any) {
    const user = {
      id: payload.id,
      name: payload.name,
      email: payload.email,
      created: payload.created,
      role: new Role(payload.role),
      remaining: payload.remaining
    } as User;

    state.user = user;
  }
};

const actions: ActionTree<AuthState, RootState> = {
  async login({ commit, dispatch }, userData) {
    await axios
      .post("/login", {
        email: userData.email,
        password: userData.password
      })
      .then(resp => {
        const user: User = resp.data.user;
        commit("SET_USER", user);
        commit("SET_AUTH", true);
        const qfrom = router.currentRoute.query.redirectFrom;
        let rto =
          typeof qfrom === "string"
            ? qfrom
            : Array.isArray(qfrom)
            ? qfrom[0]
            : "/";
        router.push(rto);

        const alert = resp.data.alert;
        const snackSuccess = {
          text: alert.message,
          type: alert.level
        } as Status;

        dispatch("snackbar", snackSuccess);
      })
      .catch(err => {
        const alert = err.response.data.alert;

        const snackErr = {
          text: alert.message,
          type: alert.level
        } as Status;

        dispatch("snackbar", snackErr);
      });
  },

  async register({ commit, dispatch }, userData) {
    await axios
      .post("/register", {
        name: userData.name,
        email: userData.email,
        password: userData.password
      })
      .then(resp => {
        const user: User = resp.data.user;
        const alert = resp.data.alert;
        commit("SET_USER", user);
        commit("SET_AUTH", true);
        router.push("/");

        const snackSuccess = {
          text: alert.message,
          type: alert.level
        } as Status;

        dispatch("snackbar", snackSuccess);
      })
      .catch(err => {
        const alert = err.response.data.alert;
        const snackErr = {
          text: alert.message,
          type: alert.level
        } as Status;
        dispatch("snackbar", snackErr);
      });
  },

  logout({ commit }) {
    const resetUser: User = {
      email: "",
      name: "",
      created: "",
      id: 0,
      role: new Role(1),
      remaining: 0
    };

    commit("SET_USER", resetUser);
    commit("SET_AUTH", false);
    commit("CLEAR_USER_VIDEOS");
    commit("CLEAR_VIDEOS");

    axios
      .head("/logout")
      .then(_ => {
        router.push("/login");
      })
      .catch(err => {
        console.log(err);
        router.push("/login");
      });
  },

  updateRemaining({ commit }, remain: number) {
    if (remain < 0) {
      remain = 0;
    }
    commit("SET_RAMAINING", remain);
  }
};

const getters: GetterTree<AuthState, RootState> = {
  isAuth: (state): boolean => {
    return state.isAuth;
  },

  isAdmin: (state): boolean => {
    return state.user.role.GetRole === ROLE.ADMIN_PERM;
  },

  getCurrentUser: (state): User => {
    return state.user;
  },

  getID: (state): number => {
    return state.user.id;
  },

  getRemaining: (state): number => {
    return state.user.remaining;
  },

  getMaxRemaining: (state): number => {
    if (state.user.role.CanReadAny) {
      return 20;
    }
    return 3;
  }
};

const module = {
  state,
  mutations,
  actions,
  getters
};

export default module;
