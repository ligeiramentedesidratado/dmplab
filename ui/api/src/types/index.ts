import { Role } from "@/utils/role";
export interface RootState {
  upload: UploadState;
  auth: AuthState;
  video: VideoState;
  snackbar: SnackState;
  progressbar: ProgressBar;
}

export interface VideoState {
  videos: Array<MediaVideo>;
}

export interface UserVideoState {
  videos: Array<MediaVideo>;
}
export interface FileState {}

export interface MediaVideo {
  id: number;
  username: string;
  created: string;
  filename: string;
  md5: string;
  size: number;
  video_link: string;
  screen_pub_link: string;
  thumb_pub_link: string;
  user_id: number;
  //
  locked: boolean;
  blob_thumb_link: string;
  blob_screen_link: string;
}

export interface User {
  id: number;
  created: string;
  name: string;
  email: string;
  role: Role;
  remaining: number;
}

export interface DecryptState {
  password: string;
  salt: string;
}

export interface AuthState {
  isAuth: boolean;
  user: User;
}

export enum StatusType {
  error = "error",
  warning = "warning",
  success = "success"
}

export interface Status {
  type: StatusType;
  text: string;
  icon?: string;
}

export interface UploadState {
  uploading: boolean;
  filename: string;
  md5: string;
  status: Status;
  progress: number;
  md5progress: number;
}

export interface SnackState {
  status: Status;
  show: boolean;
}

export interface ProgressBar {
    show: boolean;
    value: number;
}
