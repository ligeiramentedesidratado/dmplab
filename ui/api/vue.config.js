const path = require("path");

module.exports = {
  // outputDir: path.resolve(__dirname, "../dist/api/"),
  // assetsDir: "./assets",
  outputDir: path.resolve(__dirname, "../dist/api/"),
  assetsDir: "./assets",
  productionSourceMap: false,
  devServer: {
    disableHostCheck: true
  },
  transpileDependencies: ["vuetify"]
};
