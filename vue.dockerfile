FROM node:13.8.0

WORKDIR /srv/dmp

RUN mkdir -p /srv/dmp

EXPOSE 3000

CMD npm run serve
