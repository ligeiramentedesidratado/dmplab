package app

import (
	"dmplab/internal/controllers"
	"dmplab/internal/models"
	"dmplab/internal/proto/clients/config"
	"dmplab/internal/routers"
	"net/http"
	"github.com/go-chi/cors"
	"time"

	mw "dmplab/pkg/middleware"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

// AppAPI will setup and join all parts of the the app
// and construct an api end point
type AppAPI struct {
	C      *controllers.ControllerServices
	M      *models.ModelServices
	Server *http.Server
}

func NewAppAPI() *AppAPI {

	appAPI := new(AppAPI)

	mod, err := models.NewModelServices(
		models.WithDatabase(config.GetDatabasecfg().GetDialect(), config.GetDBAddr()),
		models.WithUser(config.GetKeyscfg().GetHMACKey(), config.GetKeyscfg().GetPepper()),
		models.WithVideo(),
		models.WithAbout(),
	)
	if err != nil {
		panic(err)
	}
	appAPI.M = mod

	ctrl, err := controllers.NewControllerServices(
		controllers.WithUserAPICtr(appAPI.M.User),
		controllers.WithVideoAPICtr(appAPI.M.Video, appAPI.M.User),
		controllers.WithAboutAPICtr(appAPI.M.About),
	)
	if err != nil {
		panic(err)
	}
	appAPI.C = ctrl

	if err := appAPI.setupAPIServer(); err != nil {
		panic(err)
	}

	return appAPI
}

func (a *AppAPI) setupAPIServer() error {
	router := chi.NewRouter()

	crs := cors.Handler(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins:   []string{"https://formydemons.com", "https://api.formydemons.com", "https://upload.formydemons.com","https://log.formydemons.com"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"POST","HEAD","GET","OPTIONS","PUT","DELETE"},
		AllowedHeaders:   []string{"Accept","Content-Type,Content-Length","Accept-Encoding","Authorization"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	router.Use(
		middleware.Recoverer,
		middleware.Logger,
		// cmw.Limiter,
		// lmw.Handler,
		// cmw.SecureHeader,
		// cmw.CORS,
		crs,
		mw.Auth,
		middleware.RedirectSlashes,
		mw.SetUserMw,
	)

	router.Mount("/v1/", routers.WebAPIRoutes(a.C))

	a.Server = &http.Server{
		Addr:         config.GetAPIAddr(),
		Handler:      router,
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	return nil

}

func (a *AppAPI) Close() {
	a.M.Close()
	a.Server.Close()
}
