package main

import (
	"dmplab/cmd/api/app"
	"dmplab/internal/proto/clients/config"
	"dmplab/internal/proto/clients/log"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	log.RegisterService("api")
	defer log.Close()

	dmp := app.NewAppAPI()
	defer dmp.Close()

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%v", <-c)
	}()

	log.Debugf(0, "api started: %s", config.GetAPIAddr())
	go func() {
		errs <- dmp.Server.ListenAndServe()
	}()

	log.Infof(0, "api ended: %v", <-errs)
	config.Close()
}
