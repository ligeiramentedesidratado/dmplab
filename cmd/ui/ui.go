package main

import (
	"dmplab/cmd/ui/app"
	"dmplab/internal/proto/clients/config"
	"dmplab/internal/proto/clients/log"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	log.RegisterService("ui")
	defer log.Close()

	dmp := app.NewAppUI()
	defer dmp.Close()

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%v", <-c)
	}()

	log.Debugf(0, "ui started: %s", config.GetUIAddr())
	config.Close()
	go func() {
		errs <- dmp.Server.ListenAndServe()
	}()

	log.Infof(0, "ui ended: %v", <-errs)
}
