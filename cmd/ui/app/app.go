//Package app will join all pieces together...
package app

import (
	"dmplab/internal/controllers"
	"dmplab/internal/proto/clients/config"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type AppUI struct {
	Server *http.Server
}

func NewAppUI() *AppUI {

	appUI := new(AppUI)
	router := chi.NewMux()

	router.Use(
		middleware.Recoverer,
		middleware.Logger,
	)

	controllers.FileServerSPA(router, "/")

	appUI.Server = &http.Server{
		Addr:         config.GetUIAddr(),
		Handler:      router,
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	return appUI
}

func (a *AppUI) Close() {
	a.Server.Close()
}
