FROM alpine:3.11

WORKDIR /srv/dmp

RUN apk update \
        && apk upgrade \
        && apk add --no-cache libc6-compat ca-certificates fuse curl unzip


RUN addgroup -g 1000 dmp \
        && adduser -u 1000 -G dmp -s /bin/sh -D dmp \
        && mkdir -p /srv/dmp

RUN curl -O https://downloads.rclone.org/rclone-current-linux-amd64.zip \
        && unzip rclone-current-linux-amd64.zip  \
        && mv rclone-*-linux-amd64/rclone . \
        && rm -rf rclone-* \
        && mkdir -p /srv/dmp/config \
        && mkdir -p /srv/dmp/tddata

RUN chown -R dmp:dmp /srv/dmp

EXPOSE 5572

CMD ./rclone rcd --rc-no-auth --rc-addr=0.0.0.0:5572
