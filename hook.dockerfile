FROM alpine:3.11

RUN apk update \
        && apk upgrade \
        && apk add --no-cache libc6-compat bash bc ffmpeg imagemagick ghostscript-fonts

WORKDIR /srv/dmp

RUN addgroup -g 1000 dmp \
        && adduser -u 1000 -G dmp -s /bin/sh -D dmp \
        && mkdir -p /srv/dmp/config \
        && mkdir -p /srv/dmp/tddata \
        && mkdir -p /srv/dmp/scripts

COPY ./scripts/run.sh /srv/dmp/scripts/
COPY ./deploy/hook /srv/dmp/

RUN chown -R dmp:dmp /srv
USER dmp:dmp

EXPOSE 3002

CMD sleep 9 && ./hook

