#!/bin/bash

if [ "$#" -ne 4 ]; then
    echo "Usage: $0 <video-file> <output-file-prefix> <output-thumbnail-postfix> <output-screen-postfix>"
    echo -e "\nExample: $0 cat.mp4 cat _thumb.jpeg _screen.jpeg"
    echo -e "\twill output cat_thumb.jpeg and cat_screen.jpeg or will exit with status 1."
    exit
fi

video_file=$1
prefix=$2
number_of_screenshots=9
duration=$(ffprobe -i "${video_file}" -show_entries format=duration -v quiet -of csv="p=0")
[[ ! $(echo "$duration < $number_of_screenshots" | bc) -eq 0 ]] && number_of_screenshots=3
seek_factor=$(echo "scale=5; ($duration-0.1)/($number_of_screenshots-1)" | bc)

for ss_idx in $(seq 0 $((number_of_screenshots-1))); do
    ss=$(echo "${ss_idx} * ${seek_factor}" | bc)
    if ! ffmpeg -ss "${ss}" -i "${video_file}" -vframes 1 -f image2 "${prefix}_tmp_${ss_idx}.jpeg"; then
        rm -f "${prefix}"_tmp_?.jpeg
        exit 1
    fi
done

if [[ "$number_of_screenshots" == 3 ]];then
    if ! montage "${prefix}_tmp_2.jpeg" -tile 1x1  -border 2 -bordercolor "#222222" -geometry 996x+0+0 "${prefix}_final_1.jpeg" \
        || ! convert -define jpeg:size=300x300 "${prefix}_tmp_2.jpeg"  -thumbnail '300x300^' -gravity center -extent 300x300 "${prefix}${3}" \
        || ! montage  -tile 2x1 "${prefix}"_tmp_{0,1}.jpeg -border 2 -bordercolor "#222222" -geometry 496x+0+0 "${prefix}"_final_2.jpeg \
        || ! montage "${prefix}"_final_1.jpeg "${prefix}"_final_2.jpeg -tile 1x2 -geometry +0+0 -bordercolor "#222222" "${prefix}${4}"
        then
            rm -f "${prefix}"_final_?.jpeg
            rm -f "${prefix}"_tmp_?.jpeg
            exit 1
    fi
else

    if ! montage "${prefix}_tmp_2.jpeg" -tile 1x1  -border 2 -bordercolor "#222222" -geometry 996x+0+0 "${prefix}_final_1.jpeg" \
        || ! convert -define jpeg:size=300x300 "${prefix}_tmp_2.jpeg"  -thumbnail '300x300^' -gravity center -extent 300x300 "${prefix}${3}" \
        || ! montage  -tile 2x4 "${prefix}"_tmp_{0,1,3,4,5,6,7,8}.jpeg -border 2 -bordercolor "#222222" -geometry 496x+0+0 "${prefix}"_final_2.jpeg \
        || ! montage "${prefix}"_final_1.jpeg "${prefix}"_final_2.jpeg -tile 1x2 -geometry +0+0 -bordercolor "#222222" "${prefix}${4}"
        then
            rm -f "${prefix}"_final_?.jpeg
            rm -f "${prefix}"_tmp_?.jpeg
            exit 1
    fi
fi
rm -f "${prefix}"_final_?.jpeg
rm -f "${prefix}"_tmp_?.jpeg
