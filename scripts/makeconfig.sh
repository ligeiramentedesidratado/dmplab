#!/bin/sh

[ ! -e "$1" ] && echo "invalid file $1" && exit 1
[ ! -e "$2" ] && echo "invalid file $2" && exit 1

RCFile=$1
CFGFile=$2

DST=$(cat "$CFGFile"| jq -r -e .Rclone.RRC.Dst | sed 's/:\///g' )
DSTINFO=$(rclone --config "$RCFile" config dump | jq -M -e ".$DST | {password, password2}")
jq -e --argjson groupInfo "$DSTINFO" ".Rclone.RcloneKeys = \$groupInfo" "$CFGFile" > ./config/.config
